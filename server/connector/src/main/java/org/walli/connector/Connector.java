package org.walli.connector;

import com.google.api.client.auth.oauth.OAuthHmacSigner;
import com.google.api.client.auth.oauth.OAuthParameters;
import com.google.api.client.auth.oauth.OAuthRsaSigner;
import com.google.api.client.http.GenericUrl;
import com.google.api.client.http.HttpRequest;
import com.google.api.client.http.HttpRequestFactory;
import com.google.api.client.http.javanet.NetHttpTransport;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.ObjectUtils;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.transformer.RssEntry;
import org.walli.transformer.RssXmlTransformer;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.SecureRandom;
import java.util.List;

import static javax.ws.rs.core.Response.ok;
import static org.apache.commons.io.IOUtils.closeQuietly;

/** The Connector contains utility methods for getting responses from apis. */
public final class Connector {
    private static final Logger LOG = LoggerFactory.getLogger(Connector.class);

    /** Constructor. */
    private Connector() {
        TrustManager[] trustAllCerts = new TrustManager[]{
                new TrustAllX509TrustManager()
        };

        try {
            SSLContext sc = SSLContext.getInstance("SSL");
            sc.init(null, trustAllCerts, new SecureRandom());
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Get the response from the unprotected given url.
     * @param url The url.
     * @param authenticationInformation An AuthenticationInformation implementation.
     * @return response The Response.
     */
    public static Response getResponse(final String url, final AuthenticationInformation authenticationInformation) {
        Response response;
        if (authenticationInformation instanceof OAuthRsaInformation) {
            response = getResponse(url, (OAuthRsaInformation) authenticationInformation);
        } else if (authenticationInformation instanceof OAuthHmacInformation) {
            response = getResponse(url, (OAuthHmacInformation) authenticationInformation);
        } else if (authenticationInformation instanceof BasicAuthInformation) {
            response = getResponse(url, (BasicAuthInformation) authenticationInformation);
        } else {
            response = getResponse(url);
        }

        String contentType = ObjectUtils.toString(response.getMetadata().getFirst("content-type"));
        if (StringUtils.containsIgnoreCase(contentType, "rss")) {
            LOG.debug("Got response with content-type {}, transforming to JSON instead", contentType);
            RssXmlTransformer transformer = new RssXmlTransformer();

            GenericEntity entity = (GenericEntity) response.getEntity();
            String json = entity.getEntity().toString();

            List<RssEntry> entries = transformer.transform(json);
            response = Response.status(response.getStatus()).entity(entries).build();
        }

        return response;
    }

    /**
     * Get the response from the unprotected given url.
     * @param url The url.
     * @return response The Response.
     */
    private static Response getResponse(final String url) {
        Response response;
        InputStream in = null;
        HttpURLConnection urlConnection = null;
        try {
            final URL u = new URL(url);
            urlConnection = (HttpURLConnection) u.openConnection();
            in = new BufferedInputStream(urlConnection.getInputStream());
            response = ok(new GenericEntity<String>(IOUtils.toString(in)) {
            }).type(urlConnection.getContentType()).build();
        } catch (IOException e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            closeQuietly(in);
        }
        return response;
    }

    /**
     * Get the response from the unprotected given url.
     * @param url The url.
     * @return response The Response.
     */
    private static Response getResponse(final String url, final BasicAuthInformation information) {
        Response response;
        InputStream in = null;
        HttpURLConnection urlConnection = null;
        try {
            final URL u = new URL(url);
            urlConnection = (HttpURLConnection) u.openConnection();
            final String basicAuth = "Basic " + new String(new Base64().encode(information.getUserPass().getBytes()));
            urlConnection.setRequestProperty("Authorization", basicAuth);
            in = new BufferedInputStream(urlConnection.getInputStream());
            response = ok(new GenericEntity<String>(IOUtils.toString(in)) {
            }).type(urlConnection.getContentType()).build();
        } catch (IOException e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } finally {
            if (urlConnection != null) {
                urlConnection.disconnect();
            }
            closeQuietly(in);
        }
        return response;
    }

    /**
     * Get the response from the protected given url.
     * @param url         The protected url.
     * @param information The {@link OAuthRsaInformation}.
     * @return response The response as String.
     * @throws java.io.IOException If an error occurs.
     */
    private static Response getResponse(final String url, final OAuthRsaInformation information) {
        Response response;
        try {
            final OAuthRsaSigner signer = new OAuthRsaSigner();
            signer.privateKey = information.getPrivateKey();

            final OAuthParameters parameters = new OAuthParameters();
            parameters.consumerKey = information.getConsumerKey();
            parameters.token = information.getOauthToken();
            parameters.signer = signer;
            parameters.version = "1.0";

            final NetHttpTransport netHttpTransport = new NetHttpTransport();
            final HttpRequestFactory factory = netHttpTransport.createRequestFactory(parameters);
            final GenericUrl genericUrl = new GenericUrl(url);
            final HttpRequest request = factory.buildGetRequest(genericUrl);
            final com.google.api.client.http.HttpResponse resp = request.execute();
            response = ok(new GenericEntity<String>(resp.parseAsString()) {
            }).type(resp.getContentType()).build();
        } catch (IOException e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } catch (Exception e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.serverError().build();
        }
        return response;
    }

    /**
     * Get the response from the protected given url.
     * @param url         The protected url.
     * @param information The {@link OAuthHmacInformation}.
     * @return response The response as String.
     * @throws java.io.IOException If an error occurs.
     */
    private static Response getResponse(final String url, final OAuthHmacInformation information) {
        Response response;
        try {
            final OAuthHmacSigner signer = new OAuthHmacSigner();
            signer.clientSharedSecret = information.getConsumerSecret();
            signer.tokenSharedSecret = information.getOauthSecret();

            final OAuthParameters parameters = new OAuthParameters();
            parameters.consumerKey = information.getConsumerKey();
            parameters.token = information.getOauthToken();
            parameters.signer = signer;
            parameters.version = "1.0";

            final NetHttpTransport netHttpTransport = new NetHttpTransport();
            final HttpRequestFactory factory = netHttpTransport.createRequestFactory(parameters);
            final GenericUrl genericUrl = new GenericUrl(url);
            final HttpRequest request = factory.buildGetRequest(genericUrl);
            final com.google.api.client.http.HttpResponse resp = request.execute();
            response = ok(new GenericEntity<String>(resp.parseAsString()) {
            }).type(resp.getContentType()).build();
        } catch (IOException e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.status(Status.SERVICE_UNAVAILABLE).build();
        } catch (Exception e) {
            LOG.error("An error occurred while trying to get response from url [" + url + "].", e);
            response = Response.serverError().build();
        }
        return response;
    }
}
