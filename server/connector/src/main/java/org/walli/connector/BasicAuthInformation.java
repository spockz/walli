package org.walli.connector;

/**
 * The BasicAuthInformation object holds all the information needed for setting basic security headers when making a request
 * to an basic authenticated protected resource.
 */
public class BasicAuthInformation implements AuthenticationInformation {
    private String username;
    private String password;

    /**
     * @param username
     * @param password
     */
    public BasicAuthInformation(final String username, final String password) {
        this.username = username;
        this.password = password;
    }

    /**
     * Get the username.
     * @return username The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Get the password.
     * @return password The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Get the username and password for basic auth header.
     * @return userPass The username and password in the correct representation for the basic auth header.
     */
    public String getUserPass() {
        final StringBuilder builder = new StringBuilder();
        builder.append(username);
        if (password != null) {
            builder.append(":");
            builder.append(password);
        }
        return builder.toString();
    }
}
