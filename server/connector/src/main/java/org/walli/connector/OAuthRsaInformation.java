package org.walli.connector;

import java.security.GeneralSecurityException;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.spec.PKCS8EncodedKeySpec;

import static com.google.api.client.util.Base64.decodeBase64;

/**
 * The OAuthRsaInformation object holds all the information needed for setting oauth security headers when making a request to
 * an oauth protected resource.
 */
public class OAuthRsaInformation implements AuthenticationInformation {
    private static final String BEGIN = "-----BEGIN PRIVATE KEY-----";
    private static final String END = "-----END PRIVATE KEY-----";
    private static final String RSA = "RSA";

    private String consumerKey;
    private PrivateKey privateKey;
    private String oauthToken;

    /**
     * Constructor.
     * @param consumerKey The consumer key.
     * @param privateKey  The private key.
     * @param oauthToken  The oauth token.
     */
    public OAuthRsaInformation(final String consumerKey, final String privateKey, final String oauthToken)
            throws GeneralSecurityException {
        this.consumerKey = consumerKey;
        this.privateKey = getpPrivateKey(privateKey);
        this.oauthToken = oauthToken;
    }

    /**
     * Get the consumer key.
     * @return consumerKey The consumer key.
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Get the private key.
     * @return privateKey the {@link java.security.PrivateKey}.
     */
    public PrivateKey getPrivateKey() {
        return privateKey;
    }

    /**
     * Get the oauth token.
     * @return oauthToken The oauth token.
     */
    public String getOauthToken() {
        return oauthToken;
    }

    /**
     * Get the private key from the given privateKey.
     * @param privateKey The private key.
     * @return privateKey The private key.
     * @throws java.security.GeneralSecurityException
     *          If an error occurs.
     */
    private static PrivateKey getpPrivateKey(final String privateKey) throws GeneralSecurityException {
        if (!privateKey.contains(BEGIN) || !privateKey.contains(END)) {
            throw new GeneralSecurityException("Private key is missing required BEGIN PRIVATE KEY or END PRIVATE KEY tags.");
        }
        if (privateKey.indexOf(BEGIN) + BEGIN.length() > privateKey.indexOf(END)) {
            throw new GeneralSecurityException("END PRIVATE KEY tag found before BEGIN PRIVATE KEY tag.");
        }

        final String privKey = privateKey.substring(privateKey.indexOf(BEGIN) + BEGIN.length(), privateKey.indexOf(END));
        final KeyFactory kf = KeyFactory.getInstance(RSA);
        return kf.generatePrivate(new PKCS8EncodedKeySpec(decodeBase64(privKey)));
    }
}
