package org.walli.connector;

/**
 * The OAuthHmacInformation object holds all the information needed for setting oauth security headers when making a request to
 * an oauth protected resource.
 */
public class OAuthHmacInformation implements AuthenticationInformation {
    private String consumerKey;
    private String consumerSecret;
    private String oauthToken;
    private String oauthSecret;

    /**
     * Constructor.
     * @param consumerKey    The consumer key.
     * @param consumerSecret The consumer secret.
     * @param oauthToken     The oauth token.
     * @param oauthSecret    The oauth secret.
     */
    public OAuthHmacInformation(final String consumerKey, final String consumerSecret, final String oauthToken, final String oauthSecret) {
        this.consumerKey = consumerKey;
        this.consumerSecret = consumerSecret;
        this.oauthToken = oauthToken;
        this.oauthSecret = oauthSecret;
    }

    /**
     * Get the consumer key.
     * @return consumerKey The consumer key.
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Get the consumer secret.
     * @return consumerSecret The consumer secret.
     */
    public String getConsumerSecret() {
        return consumerSecret;
    }

    /**
     * Get the oauth token.
     * @return oauthToken The oauth token.
     */
    public String getOauthToken() {
        return oauthToken;
    }

    /**
     * Get the oauth secret.
     * @return oauthSecret The oauth secret.
     */
    public String getOauthSecret() {
        return oauthSecret;
    }
}
