package org.walli.cache;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.walli.di.ConnectModule;
import org.walli.service.ISourceService;

import javax.inject.Inject;
import java.lang.reflect.Field;
import java.util.Hashtable;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.when;

/** Test class for {@link ProxyCache}. */
@RunWith(MockitoJUnitRunner.class)
public class ProxyCacheTest {
    @Inject
    private ProxyCache cache;
    @Mock
    private ISourceService service;
    @Mock
    private ProxyRequest request;
    @Mock
    private ProxyResponse response;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new ConnectModule(), new AbstractModule() {
            @Override
            protected void configure() {
                bind(ISourceService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);
    }

    @Test
    public void shouldReturnNullIfNoCachedInstanceIsFound() throws Exception {
        assertNull(cache.getCachedResponse(request));
    }

    @Test
    public void shouldReturnNullIfCachedInstanceIsExpired() throws Exception {
        cache.add(request, response);
        when(response.isValid()).thenReturn(false);
        assertNull(cache.getCachedResponse(request));
    }

    @Test
    public void shouldAddToCache() throws Exception {
        cache.add(request, response);
        assertEquals(getCacheField().size(), 1);
    }

    @Test
    public void shouldNotAddTheTwice() throws Exception {
        cache.add(request, response);
        cache.add(request, response);
        assertEquals(getCacheField().size(), 1);
    }

    @Test
    public void shouldClearCache() throws Exception {
        cache.add(request, response);
        assertEquals(getCacheField().size(), 1);
        cache.clear();
        assertEquals(getCacheField().size(), 0);
    }

    @Test
    public void shouldRemoveResponseFromCacheIfExpired() throws Exception {
        cache.add(request, response);
        assertEquals(getCacheField().size(), 1);
        when(response.isValid()).thenReturn(false);

        cache.getCachedResponse(request);

        assertEquals(getCacheField().size(), 0);
    }

    /**
     * Get the private cache field
     * @return cache The cache field.
     * @throws NoSuchFieldException
     */
    private Hashtable<ProxyRequest, ProxyResponse> getCacheField() throws NoSuchFieldException, IllegalAccessException {
        Field field = cache.getClass().getDeclaredField("cache");
        field.setAccessible(true);
        return (Hashtable)field.get(cache);
    }
}
