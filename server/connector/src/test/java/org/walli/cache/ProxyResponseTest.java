package org.walli.cache;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

/** Test class for {@link ProxyResponse}. */
@RunWith(MockitoJUnitRunner.class)
public class ProxyResponseTest {
    @Mock
    private Response response;

    @Test
    public void shouldSetValues() throws Exception {
        assertTrue(new ProxyResponse(System.currentTimeMillis() + 10000, response).isValid());
        assertFalse(new ProxyResponse(System.currentTimeMillis() - 10000, response).isValid());
        assertEquals(response, new ProxyResponse(System.currentTimeMillis(), response).getResponse());
    }
}
