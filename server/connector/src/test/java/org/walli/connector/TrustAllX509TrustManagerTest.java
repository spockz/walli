package org.walli.connector;

import org.junit.Before;
import org.junit.Test;

import static junit.framework.Assert.assertNull;

/** Test class for {@link TrustAllX509TrustManager} */
public class TrustAllX509TrustManagerTest {
    private TrustAllX509TrustManager manager; // class under test

    @Before
    public void setUp() throws Exception {
        manager = new TrustAllX509TrustManager();
    }

    @Test
    public void shouldReturnNullAcceptedIssuers() throws Exception {
        assertNull(manager.getAcceptedIssuers());
    }

    @Test
    public void shouldCheckClientTrusted() throws Exception {
        manager.checkClientTrusted(null, "");
    }

    @Test
    public void shouldCheckServerTrusted() throws Exception {
        manager.checkServerTrusted(null, "");
    }
}
