package org.walli.connector;

import junit.framework.Assert;
import org.junit.Test;

/** Test class for {@link OAuthHmacInformation} */
public class OAuthHmacInformationTest {
    private static final String CONSUMER_KEY = "consumerKey";
    private static final String CONSUMER_SECRET = "consumerSecret";
    private static final String OAUTH_TOKEN = "oauthToken";
    private static final String OAUTH_SECRET = "oauthSecret";

    @Test
    public void shouldInitialize() throws Exception {
        final OAuthHmacInformation information = new OAuthHmacInformation(CONSUMER_KEY, CONSUMER_SECRET, OAUTH_TOKEN, OAUTH_SECRET);
        Assert.assertEquals(CONSUMER_KEY, information.getConsumerKey());
        Assert.assertEquals(CONSUMER_SECRET, information.getConsumerSecret());
        Assert.assertEquals(OAUTH_TOKEN, information.getOauthToken());
        Assert.assertEquals(OAUTH_SECRET, information.getOauthSecret());
    }

}
