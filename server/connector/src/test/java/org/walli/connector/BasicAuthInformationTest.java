package org.walli.connector;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link BasicAuthInformation} */
public class BasicAuthInformationTest {
    @Test
    public void shouldInitialize() throws Exception {
        final BasicAuthInformation authInformation = new BasicAuthInformation("username", "password");
        assertEquals("username", authInformation.getUsername());
        assertEquals("password", authInformation.getPassword());
    }

    @Test
    public void shouldGetCorrectUserPass() throws Exception {
        BasicAuthInformation authInformation = new BasicAuthInformation("username", "password");
        assertEquals("username:password", authInformation.getUserPass());

        authInformation = new BasicAuthInformation("username", null);
        assertEquals("username", authInformation.getUserPass());
    }
}
