package org.walli.di;

import com.google.inject.AbstractModule;
import com.google.inject.persist.jpa.JpaPersistModule;

import java.io.IOException;
import java.util.Properties;

/** Guice Persistence module. */
public class PersistenceModule extends AbstractModule {
    @Override
    protected void configure() {
        final JpaPersistModule persistenceModule = new JpaPersistModule("walli");
        try {
            final Properties properties = new Properties();
            properties.load(getClass().getResourceAsStream("/persistence.properties"));
            persistenceModule.properties(properties);
        } catch (IOException e) {
            e.printStackTrace();
        }
        install(persistenceModule);
    }
}
