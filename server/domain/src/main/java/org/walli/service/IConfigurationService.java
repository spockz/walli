package org.walli.service;

import org.walli.domain.Configuration;

import java.util.List;

/** Service interface for configuration. */
public interface IConfigurationService {
    /**
     * Get all configuration settings.
     * @return A {@link List} of all configuration settings with their current value.
     */
    List<Configuration> configurations();

    /**
     * Gets the configuration.
     * @param id The id.
     * @return configuration The configuration
     */
    Configuration configurationById(final Long id);

    /**
     * Add a configuration.
     * @param configuration The configuration.
     */
    void add(final Configuration configuration);

    /**
     * Update a configuration.
     * @param configuration The configuration.
     */
    void update(final Configuration configuration);

    /**
     * Delete the configuration matching the given id.
     * @param id The id.
     */
    void delete(final Long id);
}
