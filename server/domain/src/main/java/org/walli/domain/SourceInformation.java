package org.walli.domain;

import org.codehaus.jackson.annotate.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlTransient;

/** The SourceInformation domain class contains information for a specific {@link Source} so data can be obtained from that source. */
@Entity
@NamedQueries({
        @NamedQuery(name = "SourceInformation.byId", query = "SELECT s FROM SourceInformation s where s.id = :id ")})
@XmlAccessorType(XmlAccessType.FIELD)
public class
        SourceInformation {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "name")
    private String name;

    @NotNull
    @Column(name = "value")
    private String value;

    @ManyToOne(targetEntity = Project.class, fetch = FetchType.LAZY, optional = false, cascade = CascadeType.ALL)
    @JoinColumn(name = "project_id", referencedColumnName = "id")
    @XmlTransient
    @JsonIgnore
    private Project project;

    /** Constructor. */
    public SourceInformation() {
    }

    /**
     * Gets the id.
     * @return id The id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the name.
     * @return name The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name The name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the value.
     * @return value The value.
     */
    public String getValue() {
        return value;
    }

    /**
     * Sets the value.
     * @param value The value.
     */
    public void setValue(final String value) {
        this.value = value;
    }

    /**
     * Gets the project.
     * @return project The project.
     */
    public Project getProject() {
        return project;
    }

    /**
     * Sets the project.
     * @param project The project.
     */
    public void setProject(final Project project) {
        this.project = project;
    }
}
