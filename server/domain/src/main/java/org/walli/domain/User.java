package org.walli.domain;

import org.hibernate.validator.constraints.Email;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/** The User domain class which represents a user account. */
@Entity
@Table(name = "`User`", uniqueConstraints = @UniqueConstraint(columnNames = "username"))
@NamedQueries({
        @NamedQuery(name = "User.findAll", query = "SELECT u FROM User u"),
        @NamedQuery(name = "User.byId", query = "SELECT u FROM User u where u.id = :id "),
        @NamedQuery(name = "User.byUsername", query = "SELECT u FROM User u where u.username = :username "),
        @NamedQuery(name = "User.admins", query = "SELECT u FROM User u where u.admin = true ")
})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class User {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(min = 3, max = 25)
    @Pattern(regexp = "[a-z0-9_-]{3,15}", message = "This is not a valid username, it must contain only letters, digits or _ and -")
    @Column(name = "username")
    private String username;

    @NotNull
    @Size(min = 3, max = 25)
    @Column(name = "firstName")
    private String firstName;

    @NotNull
    @Size(min = 3)
    @Column(name = "lastName")
    private String lastName;

    @NotNull
    @Email
    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @Column(name = "admin")
    private boolean admin = false;

    /**
     * Gets the password hash.
     * @return password The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Gets the password.
     * @param password The password.
     */
    public void setPassword(final String password) {
        this.password = password;
    }

    /**
     * Gets the id.
     * @return id The id.
     */
    public long getId() {
        return id;
    }

    /**
     * Gets the username.
     * @return username The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Gets the firstName.
     * @return firstName The firstName.
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * Sets the firstName.
     * @param firstName The firstName.
     */
    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    /**
     * Gets the lastName.
     * @return lastName The lastName.
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * Sets the lastName.
     * @param lastName The lastName.
     */
    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    /**
     * Gets the email.
     * @return email The email.
     */
    public String getEmail() {
        return email;
    }

    /**
     * Sets the email.
     * @param email The email.
     */
    public void setEmail(final String email) {
        this.email = email;
    }

    /**
     * Indicator is admin.
     * @return isAdmin Indicator is admin.
     */
    public boolean isAdmin() {
        return admin;
    }

    /**
     * Set indicator is admin.
     * @param admin Indicator is admin.
     */
    public void setAdmin(final boolean admin) {
        this.admin = admin;
    }
}
