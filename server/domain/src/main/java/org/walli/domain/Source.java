package org.walli.domain;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 * The Source domain class contains all the information such as credentials and url which are needed to establish a connection
 * to the source to fetch and obtain data.
 */
@Entity
@Table(name = "Source", uniqueConstraints = @UniqueConstraint(columnNames = "name"))
@NamedQueries({
        @NamedQuery(name = "Source.findAll", query = "SELECT s FROM Source s"),
        @NamedQuery(name = "Source.byId", query = "SELECT s FROM Source s where s.id = :id "),
        @NamedQuery(name = "Source.byName", query = "SELECT s FROM Source s where s.name = :name ")})
@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Source {
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Size(min = 1, max = 25)
    @Pattern(regexp = "[A-Za-z ]*", message = "This is not a valid name, it must contain only letters and spaces")
    @Column(name = "name")
    private String name;

    @NotNull
    @Pattern(regexp = "\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]", message = "This is not a valid url")
    @Column(name = "url")
    private String url;

    @Column(name = "authentication")
    private Authentication authentication;

    @Column(name = "cacheTime", nullable = false)
    private long cacheTime = 300000;

    // RSA
    @Column(name = "privateKey", nullable = true, length = 1000)
    private String privateKey;

    // RSA / HMAC
    @Column(name = "consumerKey", nullable = true)
    private String consumerKey;

    // RSA / HMAC
    @Column(name = "oauthToken", nullable = true)
    private String oauthToken;

    // HMAC
    @Column(name = "consumerSecret", nullable = true)
    private String consumerSecret;

    // HMAC
    @Column(name = "oauthSecret", nullable = true)
    private String oauthSecret;

    // BASIC
    @Column(name = "username", nullable = true)
    private String username;

    // BASIC
    @Column(name = "password", nullable = true)
    private String password;


    /** Constructor. */
    public Source() {
    }

    /**
     * Gets the id.
     * @return id The id.
     */
    public Long getId() {
        return id;
    }

    /**
     * Gets the name.
     * @return name The name.
     */
    public String getName() {
        return name;
    }

    /**
     * Sets the name.
     * @param name The name.
     */
    public void setName(final String name) {
        this.name = name;
    }

    /**
     * Gets the url.
     * @return url The url.
     */
    public String getUrl() {
        return url;
    }

    /**
     * Sets the url.
     * @param url The url.
     */
    public void setUrl(final String url) {
        this.url = url;
    }


    /**
     * Gets the authentication.
     * @return authentication The authentication.
     */
    public Authentication getAuthentication() {
        return authentication;
    }

    /**
     * Sets the authentication.
     * @param authentication The authentication.
     */
    public void setAuthentication(final Authentication authentication) {
        this.authentication = authentication;
    }

    /**
     * Gets the consumer key.
     * @return consumerKey The consumer key.
     */
    public String getConsumerKey() {
        return consumerKey;
    }

    /**
     * Sets the consumer key.
     * @param consumerKey The consumer key.
     */
    public void setConsumerKey(final String consumerKey) {
        this.consumerKey = consumerKey;
    }


    /**
     * Gets the consumer secret.
     * @return consumerSecret The consumer secret.
     */
    public String getConsumerSecret() {
        return consumerSecret;
    }

    /**
     * Sets the consumer secret.
     * @param consumerSecret The consumer secret.
     */
    public void setConsumerSecret(final String consumerSecret) {
        this.consumerSecret = consumerSecret;
    }

    /**
     * Gets the private key.
     * @return privateKey The private key.
     */
    public String getPrivateKey() {
        return privateKey;
    }

    /**
     * Sets the private key.
     * @param privateKey The private key.
     */
    public void setPrivateKey(final String privateKey) {
        this.privateKey = privateKey;
    }

    /**
     * Gets the oauth token.
     * @return oauthToken The oauth token.
     */
    public String getOauthToken() {
        return oauthToken;
    }

    /**
     * Sets the oauth token.
     * @return oauthToken The oauth token.
     */
    public void setOauthToken(final String oauthToken) {
        this.oauthToken = oauthToken;
    }

    /**
     * Gets the oauth secret.
     * @return oauthSecret The oauth secret.
     */
    public String getOauthSecret() {
        return oauthSecret;
    }

    /**
     * Sets the oauth secret.
     * @return oauthSecret The oauth secret.
     */
    public void setOauthSecret(final String oauthSecret) {
        this.oauthSecret = oauthSecret;
    }

    /**
     * Gets the username.
     * @return username The username.
     */
    public String getUsername() {
        return username;
    }

    /**
     * Sets the username.
     * @param username The username.
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * Gets the password.
     * @return password The password.
     */
    public String getPassword() {
        return password;
    }

    /**
     * Sets the password.
     * @param password The password.
     */
    public void setPassword(String password) {
        this.password = password;
    }

    /**
     * Gets the cache time.
     * @return cacheTime The cache time.
     */
    public long getCacheTime() {
        return cacheTime;
    }

    /**
     * Sets the cache time.
     * @param cacheTime The cache time.
     */
    public void setCacheTime(long cacheTime) {
        this.cacheTime = cacheTime;
    }
}
