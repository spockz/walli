package org.walli.di;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.persist.PersistService;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import javax.inject.Inject;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;
import java.lang.reflect.Field;
import java.util.Properties;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.powermock.api.mockito.PowerMockito.whenNew;

/** Test class for {@link PersistenceModule}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({PersistenceModule.class, Properties.class})
public class PersistenceModuleTest {
    private final ByteArrayOutputStream errContent = new ByteArrayOutputStream();
    private PrintStream originalPrintStream;

    @Mock
    private Properties properties;

    @Inject
    private PersistService service;

    @Before
    public void setUpStreams() {
        originalPrintStream = System.out;
        System.setErr(new PrintStream(errContent));
    }

    @Test
    public void shouldInjectJpaPersistService() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule());
        injector.injectMembers(this);

        assertNotNull(service);
        final Field persistenceUnitName = service.getClass().getDeclaredField("persistenceUnitName");
        persistenceUnitName.setAccessible(true);
        assertEquals("walli", persistenceUnitName.get(service));
    }

    @Test
    public void shouldThrowException() throws Exception {
        whenNew(Properties.class).withNoArguments().thenThrow(IOException.class);
        final Injector injector = Guice.createInjector(new PersistenceModule());
        injector.injectMembers(this);
        assertEquals("java.io.IOException", errContent.toString().trim());
    }

    @After
    public void cleanUpStreams() {
        System.setErr(originalPrintStream);
    }
}
