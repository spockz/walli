package org.walli.service;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Provider;
import com.google.inject.persist.PersistService;
import org.junit.Before;
import org.junit.Test;
import org.walli.di.PersistenceModule;
import org.walli.di.ServiceModule;
import org.walli.domain.Project;
import org.walli.domain.Source;
import org.walli.domain.SourceInformation;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.NoResultException;
import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/** Test class for {@link ProjectService} */
public class ProjectServiceTest {
    @Inject
    private ProjectService service; // class under test
    @Inject
    private Provider<EntityManager> provider;
    private EntityTransaction transaction;

    @Before
    public void setUp() throws Exception {
        final Injector injector = Guice.createInjector(new PersistenceModule(), new ServiceModule());
        injector.getInstance(PersistService.class).start();
        injector.injectMembers(this);
        final EntityManager em = provider.get();
        transaction = em.getTransaction();
        final Project project = new Project();
        project.setName("name");
        project.setDisplayName("display name");

        final Source sonar = new Source();
        sonar.setName("sonar");
        sonar.setUrl("http://localhost");

        final Source jenkins = new Source();
        jenkins.setName("jenkins");
        jenkins.setUrl("http://localhost");

        final SourceInformation sourceInformation = new SourceInformation();
        sourceInformation.setName("sonar");
        sourceInformation.setValue("sonar value");
        sourceInformation.setProject(project);

        project.getInfos().add(sourceInformation);

        transaction.begin();
        em.persist(sonar);
        em.persist(jenkins);

        service.add(project);
        transaction.commit();
    }

    @Test
    public void shouldGetProjects() throws Exception {
        assertEquals(1, service.projects().size());
    }

    @Test(expected = NoResultException.class)
    public void shouldThrowExceptionWhenGettingProjectByIdNotFound() throws Exception {
        service.projectById(0L, false);
    }

    @Test
    public void shouldGetProjectById() throws Exception {
        final Project project = service.projectById(1L, false);
        assertEquals(1L, project.getId(), 0);
        assertEquals("name", project.getName());
        assertEquals("display name", project.getDisplayName());
        assertEquals(1, project.getInfos().size());
    }

    @Test
    public void shouldGetProjectByIdAndAddMissingSourceInformation() throws Exception {
        final Project project = service.projectById(1L, true);
        assertEquals(1L, project.getId(), 0);
        assertEquals("name", project.getName());
        assertEquals("display name", project.getDisplayName());
        assertEquals(2, project.getInfos().size());
    }

    @Test
    public void shouldUpdateProject() throws Exception {
        Project project = service.projectById(1L, false);

        final SourceInformation sonar = new SourceInformation();
        sonar.setName("sonar");
        sonar.setValue("undated sonar value");

        final SourceInformation jenkins = new SourceInformation();
        jenkins.setName("jenkins");
        jenkins.setValue("jenkins value");

        final Set<SourceInformation> informations = new HashSet<SourceInformation>();
        informations.add(sonar);
        informations.add(jenkins);

        project.setName("updatedName");
        project.setDisplayName("updated display name");
        project.setInfos(informations);
        service.update(project);

        project = service.projectById(1L, false);
        assertEquals(1L, project.getId(), 0);
        assertEquals("updatedName", project.getName());
        assertEquals("updated display name", project.getDisplayName());
        assertEquals(2, project.getInfos().size());
        for (final SourceInformation info : project.getInfos()) {
            if (info.getName().equals("jenkins")) {
                assertEquals("jenkins value", info.getValue());
                break;
            }
        }
        for (final SourceInformation info : project.getInfos()) {
            if (info.getName().equals("sonar")) {
                assertEquals("undated sonar value", info.getValue());
                break;
            }
        }
    }

    @Test
    public void shouldDeleteProject() throws Exception {
        assertEquals(1, service.projects().size());

        transaction.begin();
        service.delete(1L);
        transaction.commit();

        assertEquals(0, service.projects().size());
    }
}

