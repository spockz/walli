package org.walli.domain;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


/** Test class for {@link Configuration}. */
public class ConfigurationTest {
    @Test
    public void shouldSetValues() throws Exception {
        final Configuration configuration = new Configuration();

        configuration.setName("name");
        configuration.setValue("value");
        configuration.setSystem(true);

        assertEquals(0L, configuration.getId(), 0);
        assertEquals("name", configuration.getName());
        assertEquals("value", configuration.getValue());
        assertTrue(configuration.isSystem());
    }
}
