package org.walli.domain;

import org.junit.Before;
import org.junit.Test;

import java.lang.reflect.Field;

import static junit.framework.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** Test class for {@link User} */
public class UserTest {
    private User user;

    @Before
    public void setUp() throws Exception {
        user = new User();
    }

    @Test
    public void shouldSetValues() throws Exception {
        user.setFirstName("firstName");
        user.setLastName("lastName");
        user.setEmail("email");
        user.setPassword("password");
        user.setAdmin(true);

        final Field username = user.getClass().getDeclaredField("username");
        username.setAccessible(true);
        username.set(user, "username");

        assertEquals(0L, user.getId(), 0);
        assertEquals("username", user.getUsername());
        assertEquals("firstName", user.getFirstName());
        assertEquals("lastName", user.getLastName());
        assertEquals("email", user.getEmail());
        assertEquals("password", user.getPassword());
        assertTrue(user.isAdmin());
    }
}