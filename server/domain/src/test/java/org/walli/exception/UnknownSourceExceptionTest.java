package org.walli.exception;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link UnknownSourceException} */
public class UnknownSourceExceptionTest {

    @Test
    public void shouldInitialize() throws Exception {
        final UnknownSourceException message = new UnknownSourceException("message");
        assertEquals("message", message.getMessage());

    }
}
