package org.walli.api;

import javax.inject.Inject;
import javax.inject.Named;
import javax.inject.Singleton;
import javax.servlet.*;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Properties;

/** The Cross-Origin Resource Sharing Filter makes it possible to call the api's from different origins. */
@Singleton
public class CORSFilter implements Filter {
    @Inject
    public
    @Named("walli")
    Properties properties;

    @Override
    public void doFilter(final ServletRequest request, final ServletResponse response, final FilterChain filterChain)
            throws IOException, ServletException {

        if (response instanceof HttpServletResponse) {
            HttpServletResponse alteredResponse = ((HttpServletResponse) response);
            addHeadersFor200Response(alteredResponse);
        }

        filterChain.doFilter(request, response);
    }

    private void addHeadersFor200Response(final HttpServletResponse response) {
        response.addHeader("Access-Control-Allow-Origin", (String) properties.get("access-control-allow-origin"));
        response.addHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE, HEAD");
        response.addHeader("Access-Control-Allow-Headers", "X-PINGOTHER, Origin, X-Requested-With, Content-Type, Accept");
        response.addHeader("Access-Control-Max-Age", "1728000");
    }

    @Override
    public void destroy() {
    }

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }
}