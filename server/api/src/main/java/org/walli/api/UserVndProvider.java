package org.walli.api;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import static org.walli.api.WalliMediaTypes.USER_VND;

/** User Vnd provider. */
@Provider
@Consumes({MediaType.APPLICATION_JSON, "text/json"})
@Produces({MediaType.APPLICATION_JSON, "text/json", USER_VND})
public class UserVndProvider extends JacksonJsonProvider {
}
