package org.walli.api;

import com.google.inject.persist.Transactional;
import org.apache.shiro.authz.annotation.RequiresAuthentication;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.walli.domain.Configuration;
import org.walli.domain.User;
import org.walli.service.IConfigurationService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.walli.api.WalliMediaTypes.CONFIGURATION_VND;
import static org.walli.api.WalliMediaTypes.USER_VND;

/** Resource that exposes the configuration api. */
@Path("/configurations")
public class ConfigurationResource {
    private static final Logger LOG = LoggerFactory.getLogger(ConfigurationResource.class);

    @Inject
    private IConfigurationService service;

    /**
     * Gets the users.
     * @return user The users.
     */
    @GET
    @Produces(CONFIGURATION_VND)
    public Response configuration() {
        LOG.debug("Getting all configuration settings.");
        final List<Configuration> configurations = service.configurations();
        return Response.ok(new GenericEntity<List<Configuration>>(configurations) {
        }).build();
    }

    @Path("/{id}")
    @GET
    @Produces(CONFIGURATION_VND)
    public Response configuration(@PathParam("id") final Long id) {
        LOG.debug("Getting user with id" + id + ".");
        final Configuration configuration = service.configurationById(id);
        return Response.ok(new GenericEntity<Configuration>(configuration) {
        }).build();
    }

    @POST
    @Consumes({CONFIGURATION_VND, MediaType.APPLICATION_JSON})
    @Produces(CONFIGURATION_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response create(final Configuration configuration) {
        LOG.debug("create configuration");
        service.add(configuration);
        return Response.ok(new GenericEntity<Configuration>(configuration) {
        }).build();
    }

    @Path("/{id}")
    @PUT
    @Consumes({CONFIGURATION_VND, MediaType.APPLICATION_JSON})
    @Produces(CONFIGURATION_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response update(@PathParam("id") final Long id, final Configuration configuration) {
        LOG.debug("Updating configuration with id" + id);
        service.update(configuration);
        return Response.ok(new GenericEntity<Configuration>(configuration) {
        }).build();
    }

    @Path("/{id}")
    @DELETE
    @Produces(CONFIGURATION_VND)
    @RequiresAuthentication
    @RequiresRoles("admin")
    @Transactional
    public Response delete(@PathParam("id") final Long id) {
        LOG.debug("Delete configuration with id" + id + ".");
        final Response response;
        if(!service.configurationById(id).isSystem()) {
            service.delete(id);
            response = Response.ok().build();
        } else {
            response = Response.status(409).entity("Cannot delete the given configuration, it is a system setting.").build();
        }
        return response;
    }
}
