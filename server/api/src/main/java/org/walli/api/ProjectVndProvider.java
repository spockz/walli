package org.walli.api;

import org.codehaus.jackson.jaxrs.JacksonJsonProvider;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;

import static org.walli.api.WalliMediaTypes.PROJECT_VND;

/** Project Vnd provider. */
@Provider
@Consumes({MediaType.APPLICATION_JSON, "text/json"})
@Produces({MediaType.APPLICATION_JSON, "text/json", PROJECT_VND})
public class ProjectVndProvider extends JacksonJsonProvider {
}
