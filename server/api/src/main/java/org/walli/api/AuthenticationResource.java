package org.walli.api;

import org.apache.shiro.SecurityUtils;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.walli.domain.User;
import org.walli.service.IUserService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;


@Path("/authentication")
public class AuthenticationResource {
    @Inject
    private IUserService service;

    /**
     * Gets the sources.
     * @return source The sources.
     */
    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public Response login(final User user) {
        final UsernamePasswordToken token = new UsernamePasswordToken(user.getUsername(), user.getPassword());
        SecurityUtils.getSubject().login(token);
        return Response.ok().build();
    }

    /**
     * Gets the sources.
     * @return source The sources.
     */
    @DELETE
    @Produces(MediaType.APPLICATION_JSON)
    public Response logout() {
        SecurityUtils.getSubject().logout();
        return Response.ok().build();
    }

    /**
     * Gets the sources.
     * @return source The sources.
     */
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response loggedIn() {
        final LoggedInStatus loggedInStatus = new LoggedInStatus();
        loggedInStatus.setStatus(SecurityUtils.getSubject().isAuthenticated());
        return Response.ok(new GenericEntity<LoggedInStatus>(loggedInStatus) {
        }).build();
    }

    @XmlRootElement
    @XmlAccessorType(XmlAccessType.FIELD)
    class LoggedInStatus {
        private Boolean status;

        /** Constructor. */
        public LoggedInStatus() {
        }

        /**
         * Gets the status.
         * @return status The status.
         */
        public Boolean getStatus() {
            return status;
        }

        /**
         * Sets the status.
         * @param status The status.
         */
        public void setStatus(Boolean status) {
            this.status = status;
        }
    }
}
