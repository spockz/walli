package org.walli.di;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.servlet.GuiceServletContextListener;

/** Guice servlet config. */
public class GuiceListener extends GuiceServletContextListener {

    @Override
    protected Injector getInjector() {
        return Guice.createInjector(
                new PersistenceModule(),
                new ServiceModule(),
                new ConnectModule(),
                new WebModule());
    }
}