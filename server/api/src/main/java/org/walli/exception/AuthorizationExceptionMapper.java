package org.walli.exception;

import com.google.inject.Singleton;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;

import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

/**
 * Authorization exceptionMapper takes care of returning the correct http status codes.
 * By default jersey will return runtime exceptions as http status code 500.
 */
@Singleton
@Provider
public class AuthorizationExceptionMapper implements ExceptionMapper<AuthorizationException> {

    @Override
    public Response toResponse(final AuthorizationException e) {
        final Response.Status status;
        final GenericEntity entity = new GenericEntity<String>(e.getMessage()) {
        };
        if (e instanceof UnauthorizedException) {
            status = Response.Status.UNAUTHORIZED;
        } else {
            status = Response.Status.FORBIDDEN;
        }
        return Response.status(status).entity(entity).build();
    }
}