package org.walli.security;

import com.google.common.collect.ImmutableSet;
import org.apache.shiro.authc.*;
import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.AuthorizationInfo;
import org.apache.shiro.authz.SimpleAuthorizationInfo;
import org.apache.shiro.realm.AuthorizingRealm;
import org.apache.shiro.subject.PrincipalCollection;
import org.walli.domain.User;
import org.walli.service.IUserService;

import javax.inject.Inject;
import java.util.Set;

/** Shiro realm. */
public class WalliShiroRealm extends AuthorizingRealm {
    @Inject
    private IUserService service;

    /** {@inheritDoc}. */
    protected AuthenticationInfo doGetAuthenticationInfo(final AuthenticationToken token) throws AuthenticationException {
        final UsernamePasswordToken upToken = (UsernamePasswordToken) token;

        final User match = service.userByUsername(upToken.getUsername());
        if (match == null) {
            throw new AccountException("No account matching the given username [" + upToken.getUsername() + "] has been found.");
        }

        if (!match.getPassword().equals(String.valueOf(upToken.getPassword()))) {
            throw new AccountException("Invalid password for the given username [" + upToken.getUsername() + "].");
        }

        return new SimpleAuthenticationInfo(upToken.getUsername(), upToken.getPassword(), this.getName());
    }

    /** {@inheritDoc}. */
    protected AuthorizationInfo doGetAuthorizationInfo(PrincipalCollection principals) {
        if (principals == null) {
            throw new AuthorizationException("PrincipalCollection method argument cannot be null.");
        }
        final String username = (String) principals.fromRealm(getName()).iterator().next();
        final User match = service.userByUsername(username);
        if (match == null) {
            throw new AccountException("No account matching the given username [" + username + "] has been found.");
        }
        Set<String> roleNames = ImmutableSet.of();
        if (match.isAdmin()) {
            roleNames = ImmutableSet.of("admin");
        }
        return new SimpleAuthorizationInfo(roleNames);
    }
}