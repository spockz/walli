package org.walli.security;

import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.apache.shiro.web.mgt.WebSecurityManager;
import org.apache.shiro.web.servlet.AbstractShiroFilter;
import org.apache.shiro.web.servlet.ShiroHttpServletRequest;

import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;

/** Shiro filter. */
@Singleton
public class WalliShiroFilter extends AbstractShiroFilter {

    @Inject
    public WalliShiroFilter(final WebSecurityManager securityManager) {
        setSecurityManager(securityManager);
    }

    @Override
    protected ServletResponse wrapServletResponse(HttpServletResponse orig, ShiroHttpServletRequest request) {
        return super.wrapServletResponse(orig, request);
    }
}