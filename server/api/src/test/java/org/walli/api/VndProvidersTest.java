package org.walli.api;

import com.sun.jersey.api.core.PackagesResourceConfig;
import com.sun.jersey.api.core.ResourceConfig;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.Produces;
import java.util.Set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/** Test class for {@link ProjectVndProvider}. */
public class VndProvidersTest {
    private Set<Class<?>> providerClasses;

    @Before
    public void setUp() throws Exception {
        final ResourceConfig config = new PackagesResourceConfig("org.walli.api");
        providerClasses = config.getProviderClasses();
    }

    @Test
    public void shouldLoadProjectVndProvider() throws Exception {
        assertTrue(providerClasses.contains(ProjectVndProvider.class));
        assertEquals(WalliMediaTypes.PROJECT_VND, ProjectVndProvider.class.getAnnotation(Produces.class).value()[2]);
    }

    @Test
    public void shouldLoadSourceVndProvider() throws Exception {
        assertTrue(providerClasses.contains(SourceVndProvider.class));
        assertEquals(WalliMediaTypes.SOURCE_VND, SourceVndProvider.class.getAnnotation(Produces.class).value()[2]);
    }

    @Test
    public void shouldLoadUserVndProvider() throws Exception {
        assertTrue(providerClasses.contains(UserVndProvider.class));
        assertEquals(WalliMediaTypes.USER_VND, UserVndProvider.class.getAnnotation(Produces.class).value()[2]);
    }

    @Test
    public void shouldLoadConfigurationVndProvider() throws Exception {
        assertTrue(providerClasses.contains(ConfigurationVndProvider.class));
        assertEquals(WalliMediaTypes.CONFIGURATION_VND, ConfigurationVndProvider.class.getAnnotation(Produces.class).value()[2]);
    }
}
