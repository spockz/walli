package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.commons.io.IOUtils;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.cache.ProxyCache;
import org.walli.cache.ProxyRequest;
import org.walli.cache.ProxyResponse;
import org.walli.cache.SourceCache;
import org.walli.connector.*;
import org.walli.domain.Authentication;
import org.walli.domain.Source;
import org.walli.service.ISourceService;
import org.walli.service.SourceService;

import javax.inject.Inject;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import java.lang.reflect.Field;
import java.util.Hashtable;

import static junit.framework.Assert.assertEquals;
import static org.mockito.Matchers.eq;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.*;

/** Test class for {@link ProxyResource}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({ProxyResource.class, SourceService.class, SourceCache.class, Connector.class})
public class ProxyResourceTest {
    private static final String NAME_OF_THE_SERVICE = "nameOfTheService";
    private static final String QUERY = "query/subquery";
    private static final String URL = "http://localhost:8000";
    private static final String CONSUMER_KEY = "consumerKey";
    private static final String OAUTH_TOKEN = "oauthToken";
    @Inject
    private ProxyResource resource;
    @Mock
    private ISourceService service;
    @Mock
    private SourceCache sourceCache;
    @Inject
    private ProxyCache proxyCache;
    @Mock
    private Source source;
    @Mock
    private Response response;

    @Before
    public void setUp() throws Exception {
        Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(ProxyResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(ISourceService.class).toInstance(service);
                bind(SourceCache.class).toInstance(sourceCache);
                bind(ProxyCache.class);
            }
        });
        injector.injectMembers(this);

        mockStatic(Connector.class);

        when(sourceCache.getSource(NAME_OF_THE_SERVICE)).thenReturn(source);
        when(source.getUrl()).thenReturn(URL);
        when(source.getPrivateKey()).thenReturn(IOUtils.toString(ProxyResourceTest.class.getResourceAsStream("/myrsakey.pk8")));
        when(source.getConsumerKey()).thenReturn(CONSUMER_KEY);
        when(source.getOauthToken()).thenReturn(OAUTH_TOKEN);
    }

//    @Test
//    public void shouldGet400ResponseBecauseTheServiceIsNotFound() throws Exception {
//        when(sourceCache.getSource(NAME_OF_THE_SERVICE)).thenReturn(null);
//        final Response response = resource.query(NAME_OF_THE_SERVICE, QUERY, );
//
//        assertEquals(400, response.getStatus());
//        assertEquals("No service with name [" + NAME_OF_THE_SERVICE + "] found", response.getEntity());
//        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
//    }
//
//    @Test
//    public void shouldCallConnectorWithoutAuthentication() throws Exception {
//        when(source.getAuthentication()).thenReturn(Authentication.none);
//        when(Connector.getResponse(eq(URL + "/" + QUERY), isA(AnonymousAuthInformation.class))).thenReturn(response);
//
//        resource.query(NAME_OF_THE_SERVICE, QUERY);
//
//        verifyStatic(times(1));
//        Connector.getResponse(eq(URL + "/" + QUERY), isA(AnonymousAuthInformation.class));
//
//        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
//    }
//
//    @Test
//    public void shouldCallConnectorWithBasicAuthentication() throws Exception {
//        when(source.getAuthentication()).thenReturn(Authentication.basic);
//        when(Connector.getResponse(eq(URL + "/" + QUERY), isA(BasicAuthInformation.class))).thenReturn(response);
//
//        resource.query(NAME_OF_THE_SERVICE, QUERY);
//
//        verifyStatic(times(1));
//        Connector.getResponse(eq(URL + "/" + QUERY), isA(BasicAuthInformation.class));
//
//        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
//    }
//
//    @Test
//    public void shouldCallConnectorWithOAuthRSAAuthentication() throws Exception {
//        when(source.getAuthentication()).thenReturn(Authentication.rsa);
//        when(Connector.getResponse(eq(URL + "/" + QUERY), isA(OAuthRsaInformation.class))).thenReturn(response);
//
//        resource.query(NAME_OF_THE_SERVICE, QUERY);
//
//        verifyStatic(times(1));
//        Connector.getResponse(eq(URL + "/" + QUERY), isA(OAuthRsaInformation.class));
//
//        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
//    }
//
//    @Test
//    public void shouldCallConnectorWithOAuthHmacAuthentication() throws Exception {
//        when(source.getAuthentication()).thenReturn(Authentication.hmac);
//        when(Connector.getResponse(eq(URL + "/" + QUERY), isA(OAuthHmacInformation.class))).thenReturn(response);
//
//        resource.query(NAME_OF_THE_SERVICE, QUERY);
//
//        verifyStatic(times(1));
//        Connector.getResponse(eq(URL + "/" + QUERY), isA(OAuthHmacInformation.class));
//
//        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
//    }

    @Test
    public void shouldNotCallConnectorWhenRequestIsCachedAndIsntExpired() throws Exception {
        final ProxyRequest<OAuthHmacInformation> proxyRequest = new ProxyRequest<OAuthHmacInformation>(URL + "/" + QUERY, new OAuthHmacInformation("", "", "", ""));
        final ProxyResponse proxyResponse = new ProxyResponse(System.currentTimeMillis() + 10000, Response.ok().build());
        proxyCache.add(proxyRequest, proxyResponse);

        when(source.getAuthentication()).thenReturn(Authentication.hmac);

        resource.query(NAME_OF_THE_SERVICE, QUERY, false);

        verifyStatic(times(0));
        Connector.getResponse(eq(URL + "/" + QUERY), isA(OAuthHmacInformation.class));

        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
    }

    @Test
    public void shouldGetTheKeepAliveState() throws Exception {
        when(source.getUrl()).thenReturn(URL + "/" + QUERY);
        when(Connector.getResponse(eq(URL), isA(AnonymousAuthInformation.class))).thenReturn(response);
        when(response.getMetadata()).thenReturn(mock(MultivaluedMap.class));

        final Response response = resource.keepalive(NAME_OF_THE_SERVICE);
        assertEquals(0, response.getStatus());

        verifyStatic(times(1));
        Connector.getResponse(eq(URL), isA(AnonymousAuthInformation.class));

        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
        verify(response.getMetadata()).putSingle("content-type", "application/json");
    }

    @Test
    public void shouldGet400ResponseBecauseTheServiceIsNotFoundWhenGettingKeepAliveState() throws Exception {
        when(sourceCache.getSource(NAME_OF_THE_SERVICE)).thenReturn(null);
        when(source.getUrl()).thenReturn(URL + "/" + QUERY);
        when(Connector.getResponse(eq(URL), isA(AnonymousAuthInformation.class))).thenReturn(response);

        final Response response = resource.keepalive(NAME_OF_THE_SERVICE);
        assertEquals(400, response.getStatus());

        verifyStatic(times(0));
        Connector.getResponse(eq(URL), isA(AnonymousAuthInformation.class));

        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);

    }

    @Test
    public void shouldGetHttpStatus500WhenTheUrlIsInvalid() throws Exception {
        when(source.getUrl()).thenReturn("invalid-url");
        when(Connector.getResponse(eq(URL), isA(AnonymousAuthInformation.class))).thenReturn(response);

        final Response response = resource.keepalive(NAME_OF_THE_SERVICE);
        assertEquals(500, response.getStatus());

        verifyStatic(times(0));
        Connector.getResponse(eq("invalid-url"), isA(AnonymousAuthInformation.class));

        verify(sourceCache, times(1)).getSource(NAME_OF_THE_SERVICE);
    }

    @Test
    public void shouldClearTheCache() throws Exception {
        final ProxyRequest<OAuthHmacInformation> proxyRequest = new ProxyRequest<OAuthHmacInformation>(URL + "/" + QUERY, new OAuthHmacInformation("", "", "", ""));
        final ProxyResponse proxyResponse = new ProxyResponse(System.currentTimeMillis() + 10000, Response.ok().build());
        proxyCache.add(proxyRequest, proxyResponse);

        assertEquals(1, getCacheField().size());

        resource.clear();

        assertEquals(0, getCacheField().size());

    }

    /**
     * Get the private cache field
     * @return cache The cache field.
     * @throws NoSuchFieldException
     */
    private Hashtable<ProxyRequest, ProxyResponse> getCacheField() throws NoSuchFieldException, IllegalAccessException {
        Field field = proxyCache.getClass().getDeclaredField("cache");
        field.setAccessible(true);
        return (Hashtable)field.get(proxyCache);
    }
}
