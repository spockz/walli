package org.walli.api;

import com.google.inject.AbstractModule;
import com.google.inject.Guice;
import com.google.inject.Injector;
import org.apache.shiro.authc.UsernamePasswordToken;
import org.apache.shiro.mgt.SecurityManager;
import org.apache.shiro.subject.Subject;
import org.apache.shiro.util.ThreadContext;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;
import org.walli.domain.User;
import org.walli.service.IUserService;
import org.walli.service.UserService;

import javax.inject.Inject;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;
import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.powermock.api.mockito.PowerMockito.mockStatic;
import static org.powermock.api.mockito.PowerMockito.when;

/** Test class for {@link AuthenticationResource}. */
@RunWith(PowerMockRunner.class)
@PrepareForTest({AuthenticationResource.class, UserService.class, ThreadContext.class, SecurityManager.class})
public class AuthenticationResourceTest {
    @Inject
    private AuthenticationResource resource;
    @Mock
    private IUserService service;
    @Mock
    private User user;
    @Mock
    private SecurityManager securityManager;
    @Mock
    private Subject subject;

    @Before
    public void setUp() throws Exception {
        mockStatic(ThreadContext.class);
        when(ThreadContext.getSecurityManager()).thenReturn(securityManager);
        when(ThreadContext.getSubject()).thenReturn(subject);

        final Injector injector = Guice.createInjector(new AbstractModule() {
            @Override
            protected void configure() {
                bind(AuthenticationResource.class); // As it is a Jax-rs resource it will be picked up.
                bind(IUserService.class).toInstance(service);
            }
        });
        injector.injectMembers(this);

    }

    @Test
    public void shouldLogin() throws Exception {
        resource.login(user);
        verify(subject, times(1)).login(isA(UsernamePasswordToken.class));
    }

    @Test
    public void shouldLogout() throws Exception {
        resource.logout();
        verify(subject, times(1)).logout();
    }

    @Test
    public void shouldIndicateLoggedIn() throws Exception {
        when(subject.isAuthenticated()).thenReturn(true);
        final Response response = resource.loggedIn();
        assertTrue(((AuthenticationResource.LoggedInStatus) ((GenericEntity) response.getEntity()).getEntity()).getStatus());
        verify(subject, times(1)).isAuthenticated();
    }

    @Test
    public void shouldIndicateNotLoggedIn() throws Exception {
        when(subject.isAuthenticated()).thenReturn(false);
        final Response response = resource.loggedIn();
        assertFalse(((AuthenticationResource.LoggedInStatus) ((GenericEntity) response.getEntity()).getEntity()).getStatus());
        verify(subject, times(1)).isAuthenticated();
    }
}
