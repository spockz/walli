package org.walli.exception;

import org.junit.Test;

import static junit.framework.Assert.assertEquals;

/** Test class for {@link Violation} */
public class ViolationTest {
    @Test
    public void shouldSetValues() throws Exception {
        final Violation violation = new Violation("key", "message");
        assertEquals("key", violation.getField());
        assertEquals("message", violation.getViolation());
    }
}