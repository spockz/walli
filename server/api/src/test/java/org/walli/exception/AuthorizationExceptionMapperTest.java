package org.walli.exception;

import org.apache.shiro.authz.AuthorizationException;
import org.apache.shiro.authz.UnauthorizedException;
import org.junit.Before;
import org.junit.Test;

import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;

/** Test class for {@link AuthorizationExceptionMapper} */
public class AuthorizationExceptionMapperTest {
    private AuthorizationExceptionMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new AuthorizationExceptionMapper();
    }

    @Test
    public void shouldMapResponseAsUnauthorized() throws Exception {
        final Response response = mapper.toResponse(new UnauthorizedException("message"));
        assertEquals(Response.Status.UNAUTHORIZED.getStatusCode(), response.getStatus());
    }

    @Test
    public void shouldMapResponseAsForbidden() throws Exception {
        final Response response = mapper.toResponse(new AuthorizationException("message"));
        assertEquals(Response.Status.FORBIDDEN.getStatusCode(), response.getStatus());
    }
}
