package org.walli.exception;

import org.hibernate.validator.engine.ConstraintViolationImpl;
import org.hibernate.validator.engine.PathImpl;
import org.junit.Before;
import org.junit.Test;

import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.junit.Assert.assertEquals;

/** Test class for{@link ConstraintViolationExceptionMapper} */
public class ConstraintViolationExceptionMapperTest {
    private ConstraintViolationExceptionMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new ConstraintViolationExceptionMapper();
    }

    @Test
    public void shouldMapResponseAsBadRequest() throws Exception {
        final Set<ConstraintViolation<?>> constraintViolations = new HashSet<ConstraintViolation<?>>();
        constraintViolations.add(new ConstraintViolationImpl("message", "iterpolatedMessage", null, null, null, "value", PathImpl.createNewPath("field"), null, null));
        final Response response = mapper.toResponse(new ConstraintViolationException("oops", constraintViolations));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final List<Violation> violations = (List<Violation>) ((GenericEntity) response.getEntity()).getEntity();
        assertEquals(1, violations.size());
        assertEquals("field", violations.get(0).getField());
        assertEquals("iterpolatedMessage", violations.get(0).getViolation());
    }

}
