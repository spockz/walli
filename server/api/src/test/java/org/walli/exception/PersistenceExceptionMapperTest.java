package org.walli.exception;

import org.junit.Before;
import org.junit.Test;

import javax.persistence.PersistenceException;
import javax.ws.rs.core.GenericEntity;
import javax.ws.rs.core.Response;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/** Test class for{@link PersistenceExceptionMapper} */
public class PersistenceExceptionMapperTest {
    private PersistenceExceptionMapper mapper;

    @Before
    public void setUp() throws Exception {
        mapper = new PersistenceExceptionMapper();
    }

    @Test
    public void shouldMapResponseAsBadRequestWhenUniqueConstraintViolationOccurred() throws Exception {
        final Response response = mapper.toResponse(new PersistenceException("message", new Throwable(new Throwable("Unique constraint, Object(field)[]"))));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final Violation violation = (Violation) ((GenericEntity) response.getEntity()).getEntity();
        assertEquals("not unique", violation.getViolation());
        assertEquals("field", violation.getField());
    }

    @Test
    public void shouldMapResponseAsBadRequestWhenSomeConstraintViolationOccurred() throws Exception {
        final Response response = mapper.toResponse(new PersistenceException("message", new Throwable(new Throwable("Constraint, Object(field)[]"))));
        assertEquals(Response.Status.BAD_REQUEST.getStatusCode(), response.getStatus());
        final Violation violation = (Violation) ((GenericEntity) response.getEntity()).getEntity();
        assertEquals("message", violation.getViolation());
        assertNull(violation.getField());
    }

}
