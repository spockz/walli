module.exports = function (config) {
    config.set({
        basePath: '../app',
        frameworks: ['jasmine'],
        files: [
            '../../common/lib/angular/angular.js',
            '../../common/lib/angular-route/angular-route.js',
            '../../common/lib/angular-cookies/angular-cookies.js',
            '../../common/lib/angular-resource/angular-resource.js',
            '../../common/lib/angular-sanitize/angular-sanitize.js',
            '../../common/lib/angular-mocks/angular-mocks.js',
            '../../.tmp/js/*.js',
            'js/walli.js',
            'js/**/*.js',
            'partials/*.html',
            '../test/mocks/stub-data.js',
            '../test/unit/setup.js',
            '../test/unit/**/*Spec.js'
        ],
        exclude: [],
        reporters: ['progress', 'junit', 'coverage'],
        preprocessors: {
            'partials/*.html': 'html2js',
            'js/**/*.js': 'coverage'
        },
        junitReporter: {
            outputFile: '../results/junit/junit.xml'
        },
        coverageReporter: {
            type: 'lcov',
            dir: '../results/coverage'
        },
        plugins : [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-coverage',
            'karma-ng-html2js-preprocessor',
            'karma-jasmine'
        ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_DISABLE,
        autoWatch: true,
        browsers: ['PhantomJS'],
        captureTimeout: 60000,
        singleRun: false
    });
};