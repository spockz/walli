"use strict";

/* jasmine specs for the source service. */
describe('The sourceService', function () {
    beforeEach(module('walli'));

    it("should get all the sources", inject(function ($httpBackend, sourceService) {
        $httpBackend.when('GET', '/api/sources?format=json').respond(stubs.services.sources, { 'Content-type': 'application/json' });

        var service = sourceService.query();
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.sources)).toBeTruthy();
        expect(service.length).toBe(3);
    }));
    it("should get a specific source", inject(function ($httpBackend, sourceService) {
        $httpBackend.when('GET', '/api/sources/1?format=json').respond(stubs.services.sources[0], { 'Content-type': 'application/json' });

        var service = sourceService.get({id:1});
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.sources[0])).toBeTruthy();
    }));
    it("should create a source", inject(function ($httpBackend, sourceService) {
        $httpBackend.when('POST', '/api/sources?format=json').respond(function(method, url, data, headers){
            var source = angular.fromJson(data);
            source.id = 4;
            stubs.services.sources.push(source);
            return [200, {}, {}];
        });

        var service = sourceService.save({"name": "nexus", "url": "http://sonatype", "authentication": "none"});
        $httpBackend.flush();

        expect(stubs.services.sources.length).toBe(4);
    }));
    it("should update a source", inject(function ($httpBackend, sourceService) {
        $httpBackend.when('PUT', '/api/sources/4?format=json').respond(function(method, url, data, headers){
            var source = angular.fromJson(data);
            source.id = 4;
            stubs.services.sources[3] = source;
            return [200, {}, {}];
        });

        var service = sourceService.update({"id": 4, "name": "nexus", "url": "http://sonatype.org", "authentication": "none"});
        $httpBackend.flush();

        expect(stubs.services.sources.length).toBe(4);
        expect(stubs.services.sources[3].url).toBe("http://sonatype.org");
    }));
    it("should delete a source", inject(function ($httpBackend, sourceService) {
        $httpBackend.when('DELETE', '/api/sources/4?format=json').respond(function(method, url, data, headers){
            stubs.services.sources.pop();
            return [200, {}, {}];
        });

        var service = sourceService.destroy({"id": 4});
        $httpBackend.flush();

        expect(stubs.services.sources.length).toBe(3);
    }));
});
