"use strict";

/* jasmine specs for the configuration service. */
describe('The configurationService', function () {
    beforeEach(module('walli'));

    it("should get all the configurations", inject(function ($httpBackend, configurationService) {
        $httpBackend.when('GET', '/api/configurations?format=json').respond(stubs.services.configurations, { 'Content-type': 'application/json' });

        var service = configurationService.query({});
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.configurations)).toBeTruthy();
        expect(service.length).toBe(2);
    }));
    it("should get a specific configuration", inject(function ($httpBackend, configurationService) {
        $httpBackend.when('GET', '/api/configurations/1?format=json').respond(stubs.services.configurations[0], { 'Content-type': 'application/json' });

        var service = configurationService.get({id:1});
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.configurations[0])).toBeTruthy();
    }));
    it("should create a configuration", inject(function ($httpBackend, configurationService) {
        $httpBackend.when('POST', '/api/configurations?format=json').respond(function(method, url, data, headers){
            var configuration = angular.fromJson(data);
            configuration.id = 3;
            stubs.services.configurations.push(configuration);
            return [200, {}, {}];
        });

        var service = configurationService.save({"name": "something", "value": "else"});
        $httpBackend.flush();

        expect(stubs.services.configurations.length).toBe(3);
    }));
    it("should update a configuration", inject(function ($httpBackend, configurationService) {
        $httpBackend.when('PUT', '/api/configurations/3?format=json').respond(function(method, url, data, headers){
            var configuration = angular.fromJson(data);
            configuration.id = 3;
            stubs.services.configurations[2] = configuration;
            return [200, {}, {}];
        });

        var service = configurationService.update({"id": 3, "name": "something", "value": "else"});
        $httpBackend.flush();

        expect(stubs.services.configurations.length).toBe(3);
        expect(stubs.services.configurations[2].value).toBe("else");
    }));
    it("should delete a configuration", inject(function ($httpBackend, configurationService) {
        $httpBackend.when('DELETE', '/api/configurations/3?format=json').respond(function(method, url, data, headers){
            stubs.services.configurations.pop();
            return [200, {}, {}];
        });

        var service = configurationService.destroy({"id": 3});
        $httpBackend.flush();

        expect(stubs.services.configurations.length).toBe(2);
    }));
});