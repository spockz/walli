"use strict";

/* jasmine specs for the authentication service. */
describe('The authenticationService', function () {
    beforeEach(module('walli'));

    it("should indicate that the user is loggedin", inject(function ($httpBackend, authenticationService) {
        $httpBackend.when('GET', '/api/authentication').respond(stubs.services.authentication, { 'Content-type': 'application/json' });

        var service = authenticationService.loggedIn();
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.authentication)).toBeTruthy();
        expect(service.status).toBeTruthy();
    }));
    it("should login", inject(function ($httpBackend, authenticationService) {
        $httpBackend.when('POST', '/api/authentication').respond(function(method, url, data, headers){
            return [200, {}, {}];
        });

        authenticationService.login({"username": "test", "password": "Test"});
        $httpBackend.flush();
    }));
    it("should logout", inject(function ($httpBackend, authenticationService) {
        $httpBackend.when('DELETE', '/api/authentication').respond(function(method, url, data, headers){
            return [200, {}, {}];
        });

        authenticationService.logout();
        $httpBackend.flush();
    }));
});
