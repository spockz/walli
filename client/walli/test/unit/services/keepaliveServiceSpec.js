"use strict";

/* jasmine specs for the keepalive service. */
describe('The keepaliveService', function () {
    beforeEach(module('walli'));

    it("should actualy call the resource", inject(function ($httpBackend, keepaliveService) {
        $httpBackend.when('GET', '/api/proxy/sonar/keepalive').respond(stubs.services.keepalive, { 'Content-type': 'application/json' });

        var service = keepaliveService.get({source: 'sonar'});
        $httpBackend.flush();

        expect(angular.equals(service, stubs.services.keepalive)).toBeTruthy();
    }));
});
