"use strict";

/* Controller that handles the updating of a project. */

angular.module('walli').controller('projectUpdateCtrl', [
    '$scope',
    '$route',
    '$routeParams',
    '$location',
    'projectService',
    'sourceService',
    'modelFactory', function ($scope, $route, $routeParams, $location, projectService, sourceService, modelFactory) {
        var self = this;

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        $scope.project = projectService.get({id: $routeParams.projectId});
        $scope.sources = sourceService.query();

        /* Indicate if the project is clean. */
        $scope.isClean = function () {
            return angular.equals(self.original, $scope.project);
        }

        /* Deletes the project. */
        $scope.destroy = function () {
            this.project.$destroy({id: $routeParams.projectId}, function () {
                $location.path('/settings');
            });
        };

        /* Updates the project. */
        $scope.save = function () {
            this.project.$update({id: $routeParams.projectId}, function () {
                $location.path('/settings');
            });
        };

    }]);