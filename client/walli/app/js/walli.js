"use strict";

angular.module('walli', ['ngResource', 'ngSanitize', 'ngCookies', 'ngRoute', 'core']);

angular.module('walli').
    config(function ($routeProvider) {
        $routeProvider.
            when('/', {
                templateUrl: 'partials/welcome.html'
            }).
            when('/projects/new', {
                controller: 'projectCreateCtrl',
                templateUrl: 'partials/project/project.html'
            }).
            when('/projects/:projectId', {
                controller: 'projectUpdateCtrl',
                templateUrl: 'partials/project/project.html'
            }).
            when('/sources/new', {
                controller: 'sourceCreateCtrl',
                templateUrl: 'partials/source/source.html'
            }).
            when('/sources/:sourceId', {
                controller: 'sourceUpdateCtrl',
                templateUrl: 'partials/source/source.html'
            }).
            when('/users/new', {
                controller: 'userCreateCtrl',
                templateUrl: 'partials/user/user.html'
            }).
            when('/users/:userId', {
                controller: 'userEditCtrl',
                templateUrl: 'partials/user/user.html'
            }).
            when('/settings', {
                templateUrl: 'partials/settings.html'
            }).
            when('/configurations/new', {
                controller: 'configurationCreateCtrl',
                templateUrl: 'partials/configuration/setting.html'
            }).
            when('/configurations/:configurationId', {
                controller: 'configurationUpdateCtrl',
                templateUrl: 'partials/configuration/setting.html'
            }).
            when('/settings/:setting', {
                templateUrl: 'partials/settings/setting.html'
            }).
            when('/login', {
                templateUrl: 'partials/user/login.html'
            }).
            when('/projects/:projectId/overview', {
                controller: 'overviewCtrl',
                templateUrl: 'partials/overview/overview.html'
            }).
            otherwise({
                redirectTo: '/'
            });
    });
