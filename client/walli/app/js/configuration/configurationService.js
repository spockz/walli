"use strict";

/* Configuration service. Used to maintain configuration settings, not to query actual configuration. */

angular.module('walli').factory('configurationService', ['$resource', function ($resource) {
    return $resource('/api/configurations/:id', {
        format: 'json',
        id: '@id'}, {
        query: {
            method: 'GET',
            isArray: true
        },
        update: {method: 'PUT'},
        save: {method: 'POST'},
        destroy: {method: 'DELETE'}
    });
}]);