"use strict";

/* Controller that gets all the configurations. */
angular.module('walli').controller('configurationsCtrl', [
    '$scope',
    'configurationService',
    'modelFactory', function ($scope, configurationService, modelFactory) {
        $scope.configurations = configurationService.query({}, function (configurations) {

            var configurationsConfig = modelFactory.createDatatableConfig()
                .addColumn('name', 'Setting', '', '{{row.name}}', true)
                .addColumn('value', 'Value', '', '{{row.value}}', true)
                .addColumn('', 'Opts', 'icon', '<a href="#/configurations/{{row.id}}"><i class="icon-pencil"></i></a>', true)
                .addRows($scope.configurations);

            $scope.configurationsConfig = configurationsConfig;
        }, function (response) {
        });
    }]);