"use strict";

/* Controller that handles the creation of a configuration. */

/* global _ */
angular.module('walli').controller('configurationCreateCtrl', [
    '$scope',
    '$q',
    '$location',
    'configurationService',
    'modelFactory', function ($scope, $q, $location, configurationService, modelFactory) {
        $scope.configuration = {};

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        /* Indicate the configurationname is unique. */
        $scope.uniqueConfiguration = function () {
            var defer = $q.defer();
            configurationService.query({}, function (configurations) {
                var found = _.find(configurations, function (c) {
                    return c.name === $scope.configuration.name;
                });
                defer.resolve(found === undefined);
            });
            return defer.promise;
        }

        /* Save the source. */
        $scope.save = function () {
            configurationService.save($scope.configuration, function () {
                $location.path('/settings');
            }, function (response) {
                $scope.configurationForm.configurationname.$setValidity('unique', false);
            });
        }

        /* If the scope changes the form should take care of validation. */
        $scope.$watch($scope, function () {
            $scope.configurationForm.configurationname.$setValidity('unique', true);
        })
    }]);
