"use strict";

/* Controller that handles connecting with and processing data from sonar. */

/* global _, $ */
angular.module('walli').controller('sonarCtrl', [
    '$scope',
    '$routeParams',
    '$q',
    'projectService',
    'proxyService',
    'userPreferenceService',
    'modelFactory', function ($scope, $routeParams, $q, projectService, proxyService, userPreferenceService, modelFactory) {
        $scope.online = true;

        $scope.selectCodeQualityTab = function (name) {
            var projectPreferences = userPreferenceService.retrieveProjectPreferences($routeParams.projectId);
            angular.forEach(projectPreferences.sonar.viewMode, function (mode) {
                mode.checked = mode.name === name;
            });
            userPreferenceService.storeProjectPreferences($routeParams.projectId, projectPreferences);
            $('#codeQuality a:last').tab('show')
        }

        /* Get the current quality view from the localStorage. */
        $scope.codeQualityView = function () {
            var projectPreferences = userPreferenceService.retrieveProjectPreferences($routeParams.projectId);

            var found = _.find(projectPreferences.sonar.viewMode, function (mode) {
                return mode.checked
            });

            if (!angular.isDefined(found)) {
                projectPreferences.sonar.viewMode = [
                    {name: 'overview', checked: true},
                    {name: 'details', checked: false}
                ];

                found = projectPreferences.sonar.viewMode[0];
                userPreferenceService.storeProjectPreferences($routeParams.projectId, projectPreferences);
            }

            return found.name;
        }

        $scope.$on('project', function (event, project) {
            var source = _.filter(project.infos, function (info) {
                return info.name === 'sonar'
            })[0];

            if (source !== undefined && !_.isEmpty(source.value)) {
                var sonarId = source.value;

                fetch(sonarId, false);
                $scope.$on('refresh', function () {
                    fetch(sonarId, true);
                });
            }
        });

        /**
         * Fetch the actual data.
         * @param sonarId The sonar id.
         * @param refresh Indicator refresh.
         */
        function fetch(sonarId, refresh) {
            $q.all([
                    getGageMetrics(sonarId, 'coverage,line_coverage,branch_coverage,violations_density,test_success_density', refresh),
                    getGageMetrics(sonarId, 'technical_debt_ratio', refresh)])
                .then(function (data) {
                    var qualityGauges = data[0];
                    if (angular.isDefined(data[1][0])) {
                        var technicalDebtRatioData = data[1][0];
                        technicalDebtRatioData.min = 0;
                        technicalDebtRatioData.max = 5;
                        technicalDebtRatioData.levelColors = ['#a9d70b', '#a9d70b', '#f9c802', '#ff0000', '#ff0000']
                        qualityGauges.push(technicalDebtRatioData);
                    }
                    $scope.qualityGauges = qualityGauges;

                    $scope.online = true;
                }, function (reason) {
                    $scope.online = false;
                });

            $q.all([
                    getDetailMetrics(sonarId, 'tests,test_failures,test_errors,skipped_tests', refresh),
                    getDetailMetrics(sonarId, 'violations,blocker_violations,critical_violations,major_violations,minor_violations,info_violations', refresh),
                    getDetailMetrics(sonarId, 'technical_debt_complexity,technical_debt_repart,technical_debt_days,technical_debt', refresh),
                    getDetailMetrics(sonarId, 'ncloc,lines,statements,files,classes,functions,accessors', refresh)
                ])
                .then(function (data) {
                    $scope.unitTestConfig = data[0];
                    $scope.rulesComplianceConfig = data[1];
                    $scope.technicalDebtConfig = data[2];
                    $scope.projectDetailConfig = data[3];
                    $scope.online = true;
                }, function (reason) {
                    $scope.online = false;
                });
        }

        /**
         * Get the detail metrics for the given sonar id.
         * @param sonarId The sonar id.
         * @param metrics The metrics to fetch.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function getDetailMetrics(sonarId, metrics, refresh) {
            var deferred = $q.defer();
            var m = [];

            var success = function (data) {
                angular.forEach(data[0].msr, function (metric) {
                    m.push(getMetric(metric, refresh));
                });
                $q.all(m)
                    .then(function (data) {
                        var config = modelFactory.createDatatableConfig()
                            .addColumn('title', 'Metric', '', '{{row.title}}', true)
                            .addColumn('value', 'Value', '', '{{row.value}}', true)
                            .addRows(data);

                        deferred.resolve(config);
                    });
            };

            var error = function (response) {
                deferred.reject();
            };

            if(refresh) {
                proxyService.getRefresh({
                    source: 'sonar',
                    query: 'resources?resource=' + sonarId + '&metrics=' + metrics + '&format=json'
                }, success, error);
            } else {
                proxyService.get({
                    source: 'sonar',
                    query: 'resources?resource=' + sonarId + '&metrics=' + metrics + '&format=json'
                }, success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the gage metrics for the given sonar id.
         * @param sonarId The sonar id.
         * @param metrics The metrics to fetch.
         * @param refresh Indicator refresh.
         * @returns {*}
         */
        function getGageMetrics(sonarId, metrics, refresh) {
            var deferred = $q.defer();
            var m = [];

            var success = function (data) {
                if (data[0] !== undefined) {
                    angular.forEach(data[0].msr, function (metric) {
                        m.push(getMetric(metric, refresh));
                    });
                }
                $q.all(m)
                    .then(function (data) {
                        var metrics = [];
                        angular.forEach(data, function (metric) {
                            metrics.push(modelFactory.createJustGageData().identifier(metric.key).value(metric.value).title(metric.title).data);
                        })
                        deferred.resolve(metrics);
                    });
            };

            var error = function (response) {
                deferred.reject();
            };

            if(refresh) {
                proxyService.getRefresh({
                    source: 'sonar',
                    query: 'resources?resource=' + sonarId + '&metrics=' + metrics + '&format=json'
                }, success, error);
            } else {
                proxyService.get({
                    source: 'sonar',
                    query: 'resources?resource=' + sonarId + '&metrics=' + metrics + '&format=json'
                }, success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the metric.
         * @param metric The metric.
         * @param refresh Indicator refresh.
         * @returns {*}
         */
        function getMetric(metric, refresh) {
            var deferred = $q.defer();

            var success = function (data) {
                deferred.resolve({key: metric.key, value: metric.val, title: data[0].name});
            };

            var error = function (response) {
                deferred.reject();
            };
            if(refresh) {
                proxyService.getRefresh({
                    source: 'sonar',
                    query: 'metrics/' + metric.key
                }, success, error);
            } else {
                proxyService.get({
                    source: 'sonar',
                    query: 'metrics/' + metric.key
                }, success, error);
            }

            return deferred.promise;
        }
    }]);
