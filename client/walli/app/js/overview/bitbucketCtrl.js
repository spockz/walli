"use strict";

/* Controller that handles connecting with and processing data from bitbucket. */

/* global _, $ */
angular.module('walli').controller('bitbucketCtrl', [
    '$scope',
    '$routeParams',
    '$q',
    'projectService',
    'proxyService',
    'userPreferenceService',
    'modelFactory',
    'utils', function ($scope, $routeParams, $q, projectService, proxyService, userPreferenceService, modelFactory, utils) {
        var projectId = $routeParams.projectId;
        $scope.online = true;
        $scope.source = "bitbucket";

        var tooltip = document.createElement('div'),
            leftOffset = -($('html').css('padding-left').replace('px', '') + $('body').css('margin-left').replace('px', '')),
            topOffset = -32;
        tooltip.className = 'ex-tooltip';
        document.body.appendChild(tooltip);

        $scope.$on('project', function (event, project) {
            var source = _.filter(project.infos, function (info) {
                return info.name === 'bitbucket'
            })[0];

            if (source !== undefined && !_.isEmpty(source.value)) {
                var bitbucket = source.value.split("/")
                var bitbucketAccountName = bitbucket[0];
                var bitbucketRepository = bitbucket[1];

                fetch(bitbucketAccountName, bitbucketRepository, false);

                $scope.fetchCommits = function () {
                    commitsData(bitbucketAccountName, bitbucketRepository, $scope.branch, false).then(function (response) {
                        var commitActivityData = modelFactory.createXChartData().yScale('linear');
                        angular.forEach(response.data, function (d) {
                            commitActivityData.addDataEntry(d.date, d.total);
                        });
                        $scope.commitActivityData = commitActivityData.data;
                    });
                    var projectPreferences = userPreferenceService.retrieveProjectPreferences(projectId);
                    projectPreferences.bitbucket.branch = $scope.branch;
                    userPreferenceService.storeProjectPreferences(projectId, projectPreferences);
                };

                $scope.$on('refresh', function () {
                    fetch(bitbucketAccountName, bitbucketRepository, true);
                });
            }
        });

        /**
         * Fetch the actual data.
         * @param bitbucketAccountName The bitbucket account name.
         * @param bitbucketRepository The bitbucket repository.
         * @param refresh Indicator refresh.
         */
        function fetch(bitbucketAccountName, bitbucketRepository, refresh) {
            branchesData(bitbucketAccountName, bitbucketRepository, refresh).then(function (response) {
                var defaultBranch = _.find(response.branches, function (branch) {
                    return branch.name === 'master';
                });
                var currentBranch = userPreferenceService.retrieveProjectPreferences(projectId).bitbucket.branch;
                var currentBranchName = angular.isDefined(currentBranch) ? currentBranch.name : undefined;

                var found = _.find(response.branches, function (branch) {
                    return branch.name === currentBranchName;
                });
                var projectPreferences = userPreferenceService.retrieveProjectPreferences(projectId);

                if (!found) {
                    projectPreferences.bitbucket.branch = {name: defaultBranch.name};
                    $scope.branch = defaultBranch;
                } else {
                    projectPreferences.bitbucket.branch = found;
                    $scope.branch = found;
                }
                userPreferenceService.storeProjectPreferences(projectId, projectPreferences);

                commitsData(bitbucketAccountName, bitbucketRepository, $scope.branch, refresh).then(function (response) {
                    var commitActivityData = modelFactory.createXChartData().yScale('linear');
                    angular.forEach(response.data, function (d) {
                        commitActivityData.addDataEntry(d.date, d.total);
                    });
                    $scope.commitActivityData = commitActivityData.data;
                });

                $scope.branches = response.branches;

                $scope.commitActivityChartOptions = modelFactory.createXChartOptions()
                    .paddingLeft(35)
                    .paddingRight(10)
                    .paddingTop(10)
                    .axisPaddingLeft(5)
                    .tickHintY(4)
                    .mouseover(function (d, i) {
                        var pos = $(this).offset();
                        $(tooltip).text("Commit activity on " + d.x + " is: " + d.y)
                            .css({top: topOffset + pos.top, left: pos.left + leftOffset})
                            .show();
                    })
                    .mouseout(function (x) {
                        $(tooltip).hide();
                    })
                    .options;
                $scope.online = true;

            }, function (response) {
                $scope.online = false;
            });
        }

        /**
         * Get the commits.
         * @param accountName The account name.
         * @param repository The repository.
         * @param branch The branch.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function commitsData(accountName, repository, branch, refresh) {
            var deferred = $q.defer();

            var success = function (data) {
                deferred.resolve({name: repository, data: getTimelineData(data.values)});
            };

            var error = function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.queryRefresh({
                    source: 'bitbucket',
                    query: 'api/2.0/repositories/' + accountName + '/' + repository + '/commits/' + branch.name
                }, success, error);
            } else {
                proxyService.query({
                    source: 'bitbucket',
                    query: 'api/2.0/repositories/' + accountName + '/' + repository + '/commits/' + branch.name
                }, success, error);
            }
            return deferred.promise;
        }

        /**
         * Get the branches.
         * @param accountName The account name.
         * @param repository The repository.
         * @param refresh Indicator refresh.
         * @return {*}
         */
        function branchesData(accountName, repository, refresh) {
            var deferred = $q.defer();

            var success = function (data) {
                var branches = [];
                angular.forEach(data, function (branch) {
                    if (angular.isDefined(branch.branch)) {
                        branches.push({name: branch.branch});
                    }
                });
                deferred.resolve({name: repository, branches: branches});
            };

            var error = function (response) {
                deferred.reject({failed: true, status: response.status});
            };

            if (refresh) {
                proxyService.queryRefresh({
                    source: 'bitbucket',
                    query: 'api/1.0/repositories/' + accountName + '/' + repository + '/branches'
                }, success, error);
            } else {
                proxyService.query({
                    source: 'bitbucket',
                    query: 'api/1.0/repositories/' + accountName + '/' + repository + '/branches'
                }, success, error);
            }

            return deferred.promise;
        }

        /**
         * Get the timeline data.
         * @param commitData The commit data.
         * @return {Array}
         */
        function getTimelineData(commits) {

            var commitData = [],
                timelineData = [];
            // commits by date.
            angular.forEach(commits, function (commit) {
                commitData.push({
                    date: utils.dayTimestamp(commit.date)
                });
            });

            var groupedByDate = _.chain(commitData).groupBy('date').sortBy(function (commit) {
                return commit.date;
            }).value();

            angular.forEach(groupedByDate, function (commitsByDate) {
                timelineData.push({date: utils.fullDate(commitsByDate[0].date), total: _.groupBy(commitsByDate, 'name').undefined.length});
            });
            return timelineData.splice(0, 15);
        }
    }]);
