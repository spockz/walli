"use strict";

/* Source service. */

angular.module('walli').factory('sourceService', ['$resource', function ($resource) {
    return $resource('/api/sources/:id', {
        format: 'json',
        id: '@id'}, {
        query: {
            method: 'GET',
            isArray: true
        },
        update: {method: 'PUT'},
        save: {method: 'POST'},
        destroy: {method: 'DELETE'}
    });
}]);