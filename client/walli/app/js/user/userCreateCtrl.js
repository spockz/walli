"use strict";

/* Controller that handles the creation of a user. */

/* global _ */
angular.module('walli').controller('userCreateCtrl', [
    '$scope',
    '$q',
    '$location',
    'userService',
    'modelFactory', function ($scope, $q, $location, userService, modelFactory) {
        $scope.user = {};

        $scope.stateConfig = modelFactory.createStateConfig()
            .addState(true, 'icon-ok status-success')
            .addState(false, 'icon-remove status-error', true);

        /* Indicate the username is unique. */
        $scope.uniqueUser = function () {
            var defer = $q.defer();
            userService.query({}, function (users) {
                var found = _.find(users, function (u) {
                    return u.username === $scope.user.username;
                });
                defer.resolve(found === undefined);
            });
            return defer.promise;
        }

        /* Save the source. */
        $scope.save = function () {
            userService.save($scope.user, function () {
                $location.path('/settings');
            }, function (response) {
                $scope.userForm.username.$setValidity('unique', false);
            });
        }

        /* If the scope changes the form should take care of validation. */
        $scope.$watch($scope, function () {
            $scope.userForm.username.$setValidity('unique', true);
        })
    }]);
