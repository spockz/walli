"use strict";

/* The version service. */

angular.module('walli').factory('versionService', function () {
    return {
        version: '0.1.0-SNAPSHOT'
    };
});