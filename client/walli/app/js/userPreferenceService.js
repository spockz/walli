"use strict";

/* User preference service. */

angular.module('walli').factory('userPreferenceService', function () {
    var retrieveProjectsPreferences = function () {
        var preferences = {};
        if (localStorage.getItem('walli-projects-prefs') === null) {
            localStorage.setItem('walli-projects-prefs', angular.toJson(preferences));
        } else {
            preferences = angular.fromJson(localStorage.getItem('walli-projects-prefs'));
        }
        return preferences;
    };

    return {
        retrieveProjectsPreferences: retrieveProjectsPreferences,
        retrieveProjectPreferences: function (key) {
            var preferences = retrieveProjectsPreferences();
            var projectPreferences = preferences[key];
            return angular.isDefined(projectPreferences) ? projectPreferences : {};
        },
        storeProjectPreferences: function (key, value) {
            var preferences = retrieveProjectsPreferences();
            preferences[key] = value;
            localStorage.setItem('walli-projects-prefs', angular.toJson(preferences));
        }
    }
});
