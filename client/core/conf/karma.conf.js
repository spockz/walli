module.exports = function (config) {
    config.set({
        basePath: '../app',
        frameworks: ['jasmine'],
        files: [
            '../../common/lib/angular/angular.js',
            '../../common/lib/angular-cookies/angular-cookies.js',
            '../../common/lib/angular-resource/angular-resource.js',
            '../../common/lib/angular-sanitize/angular-sanitize.js',
            '../../common/lib/angular-mocks/angular-mocks.js',
            '../../common/lib/jquery/jquery.js',
            '../../common/lib/bootstrap-sass/js/modal.js',
            '../../common/lib/justgage/justgage.js',
            '../../common/lib/bower-xcharts/xcharts.js',
            '../../common/lib/d3/d3.js',
            '../../common/lib/raphael/raphael.js',
            '../../common/lib/gritter/js/jquery.gritter.js',
            'js/core.js',
            'js/**/*.js',
            'partials/*.html',
            '../test/unit/**/*Spec.js'
        ],
        exclude: [],
        reporters: ['progress', 'junit','coverage'],
        preprocessors: {
            'partials/**/*.html': 'ng-html2js',
            'js/**/*.js': 'coverage'
        },
        junitReporter: {
            outputFile: '../results/junit/junit.xml'
        },
        coverageReporter: {
            type: 'lcov',
            dir: '../results/coverage'
        },
        plugins : [
            'karma-junit-reporter',
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-coverage',
            'karma-ng-html2js-preprocessor',
            'karma-jasmine'
        ],
        port: 9876,
        colors: true,
        logLevel: config.LOG_DISABLE,
        autoWatch: true,
        browsers: ['PhantomJS'],
        captureTimeout: 60000,
        singleRun: false
    });
};