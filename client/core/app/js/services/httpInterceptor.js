"use strict";

/**
 * The httpInterceptor intercepts requests and responses and manipulates them accordingly.
 * If a response is not 200 it will add a notification to the notification service.
 */

/* global notificationService */
angular.module('core').factory('httpInterceptor', [
    '$q',
    'notificationService',
    '$location',
    function ($q, notificationService, $location) {
        return {
            'response': function (response) {
                if (response.config.url.indexOf('.html') > 0) {
                    return response;
                }
                else if (angular.isUndefined(response.headers()['content-type']) || response.headers()['content-type'].indexOf("json") === -1) {
                    response.status = 500;
                    return $q.reject(response);
                } else {
                    return response;
                }
            },
            'responseError': function (rejection) {
                if (rejection.status === 0) {
                    notificationService.addNotification('error', 'Backend offline.', '<strong>Walli</strong> was unable to access the server for you. It is currently unavailable');
                } else if (rejection.status === 401) {
                    notificationService.addNotification('error', 'Unauthorized.', 'You do not have the correct authorization.');
                } else if (rejection.status === 403) {
                    notificationService.addNotification('error', 'Authentication required.', 'You are not authenticated. Please login.');
                    $location.path("/login")
                } else if (rejection.status >= 400 && rejection.status < 500) {
                    notificationService.addNotification('error', 'Something went wrong.', 'Walli was unable to find what you are looking for... Sorry!!!');
                } else if (rejection.status === 503) {
                    var url = rejection.config.url;
                    var result = url;
                    var startIndex = url.indexOf("/api/proxy/");
                    if (startIndex > 0) {
                        result = url.substr(startIndex + 10, url.length);
                    }
                    notificationService.addNotification('error', 'Service unavailable.', '' +
                        '<strong>Walli</strong> was unable to access the service [<span class="status-error" style="font-size: 120%;"><strong>' + result + '</strong></span>] for you. It is currently unavailable');
                }
                return $q.reject(rejection);
            }
        };
    }]);