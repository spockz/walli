"use strict";

angular.module('core', []);
angular.module('core').config(['$httpProvider', function ($httpProvider) {
    $httpProvider.interceptors.push('httpInterceptor');
}]);

