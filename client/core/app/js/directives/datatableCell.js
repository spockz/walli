"use strict";

/* The datatable cell directive. */

angular.module('core').directive('datatableCell', ['$compile', function ($compile) {
    return {
        restrict: 'E',
        transclude: true,
        scope: true,
        replace: true,
        link: function (scope, element, attrs) {
            var template = scope.$eval(attrs.template);
            var config = scope.$eval(attrs.config);
            var row = scope.$eval(attrs.row);

            scope.config = config;
            scope.row = row;

            element.html(template);
            element.append($compile(element.contents())(scope));
        }
    }
}]);
