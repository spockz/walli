"use strict";

/* jasmine specs for the notification directive, */
describe('The notification directive', function () {
    var scope, rootScope, element, controller, service;

    beforeEach(function() {
        module('core');
        this.addMatchers({

            toHaveBeenCalledWithConfig: function(expectedConfig) {
                var actual = this.actual.argsForCall[0][0];
                var match = true;
                if(actual.key !== expectedConfig.key) {
                    match = false;
                }
                if(actual.title !== expectedConfig.title) {
                    match = false;
                }
                if(actual.text !== expectedConfig.text) {
                    match = false;
                }
                if(expectedConfig.sticky === undefined) {
                    if(actual.sticky !== false) {
                        match = false;
                    }
                } else if(actual.sticky !== expectedConfig.sticky){
                    match = false;
                }
                if(actual.time !== expectedConfig.time) {
                    match = false;
                }
                if(actual.class_name !== expectedConfig.class_name) {
                    match = false;
                }

                return match;
            }

        });
    });


    describe('that is provided with a valid config', function() {
        beforeEach(inject(function ($rootScope, $compile) {
            rootScope = $rootScope;
            scope = $rootScope.$new();

            scope.not = {
                key: 'key',
                title: 'title',
                text: 'text',
                sticky: false
            };

            spyOn($.gritter, 'add').andCallThrough();

            element = angular.element(
                '<notification key="not.key" title="not.title" text="not.text" sticky="not.sticky""></notification>'
            );
            $compile(element)(scope);
            scope.$digest();
        }));

        it('should add gritter', function () {
            var result = {
                key: scope.not.key,
                title: scope.not.title,
                text: scope.not.text,
                sticky: scope.not.sticky,
                time: '',
                class_name: undefined

            };
            expect($.gritter.add).toHaveBeenCalledWithConfig(result);
        });
    });

    describe('that is provided with an empty config', function() {
        beforeEach(inject(function ($rootScope, $compile) {
            rootScope = $rootScope;
            scope = $rootScope.$new();

            scope.not = {
                key: undefined,
                title: undefined,
                text: undefined,
                sticky: undefined,
                time: '200',
                className: 'className'
            };

            spyOn($.gritter, 'add').andCallThrough();

            element = angular.element(
                '<notification key="not.key" title="not.title" text="not.text" sticky="not.sticky" time="not.time" class-name="not.className"></notification>'
            );
            $compile(element)(scope);
            scope.$digest();
        }));

        it('should add gritter', function () {
            var result = {
                key: scope.not.key,
                title: scope.not.title,
                text: scope.not.text,
                sticky: scope.not.sticky,
                time: scope.not.time,
                class_name: scope.not.className

            };
            expect($.gritter.add).toHaveBeenCalledWithConfig(result);
        });
    });

    describe('that is closed', function() {
        beforeEach(inject(function ($rootScope, $compile, $controller, notificationService) {
            rootScope = $rootScope;
            scope = $rootScope.$new();
            service = notificationService;

            service.addNotification('key', 'title', 'message');

            spyOn(notificationService, 'removeNotification').andCallThrough();

            scope.key = service.getNotifications()[0].key;

            controller = $controller('notificationCtrl', {
                $scope: scope, notificationService: service
            });
        }));

        it('should notify service', function () {
            scope.afterClose({});
            expect(service.removeNotification).toHaveBeenCalledWith(service.getNotifications()[0].key);
        });
    });

});
