"use strict";

/* jasmine specs for the chart directive, */
describe('The Chart directive', function () {
    var scope, compile, factory, xChart;

    beforeEach(function () {
        module('core');

        inject(function($rootScope, $compile, modelFactory) {
            scope = $rootScope;
            compile = $compile;
            factory = modelFactory;
            xChart = jasmine.createSpy('xChart');
            spyOn(window, 'xChart').andReturn(xChart);

            scope.identifier = 'identifier';
            scope.data = factory.createXChartData()
                .yScale('linear')
                .addDataEntry('title', 120351/60000)
                .data;

            scope.options = factory.createXChartOptions()
                .xMin(1)
                .xMax(2)
                .options;
        });
    });

    describe('that is provided with valid data', function() {
        var element;

        beforeEach(function() {
            element = angular.element(
                '<div>' +
                '   <chart identifier="identifier" data="data" type="bar" options="options"></chart>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();
        });

        it("should set the correct style class", function() {
            var div = element.find('div');
            expect(div.length).toBe(1);
            expect(div.eq(0).attr('class')).toContain('xchart xcharts-line-dotted');
        });

        it("should set the correct id", function() {
            var div = element.find('div');
            expect(div.eq(0).attr('id')).toEqual('identifier');
        });

        it("should call the xChart constructor with valid data", function() {
            expect(window.xChart).toHaveBeenCalledWith('bar', { xScale : 'ordinal', yScale : 'linear', main : [ { className : '.pizza', data : [ { x : 'title', y : 2.00585 } ] } ] }, '#identifier', { xMin : 1, xMax : 2 });
        })
    });

    describe('that is provided without a chart type', function() {
        var element;

        beforeEach(function() {
            element = angular.element(
                '<div>' +
                '   <chart identifier="identifier" data="data" options="options"></chart>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();

        });

        it("should still call the xChart constructor with a valid data and use type line-dotted as it is the default", function() {
            expect(window.xChart).toHaveBeenCalledWith('line-dotted', { xScale : 'ordinal', yScale : 'linear', main : [ { className : '.pizza', data : [ { x : 'title', y : 2.00585 } ] } ] }, '#identifier', { xMin : 1, xMax : 2 });
        })
    });

    describe('that is not provided with any data', function() {
        var element;

        beforeEach(function() {
            element = angular.element(
                '<div>' +
                '   <chart identifier="identifier" options="options"></chart>' +
                '</div>'
            );

            compile(element)(scope);
            scope.$digest();

        });

        it("should not call the xChart constructor", function() {
            expect(window.xChart).not.toHaveBeenCalled();
        })
    });
});
