"use strict";

/* jasmine specs for the unique directive, */
describe('The unique directive', function () {
    var scope, element, form, q;

    beforeEach(function() {
        module('core');

        inject(function($rootScope, $compile, $q) {
            scope = $rootScope;
            q = $q;
            scope.model = {example : "value"};

            scope.unique = function() {
                var deferred = q.defer();
                deferred.resolve(scope.result);
                return deferred.promise;
            }

            element = angular.element(
                '<form name="form">' +
                '   <input type="text" name="example" id="example" ng-model="model.example" unique="unique()">' +
                '</form>'
            );

            $compile(element)(scope);
            scope.$digest();
            form = scope.form;
        })
    });

    it('should invalidate value as it is not unique', function () {
        scope.result = false;
        element.find('input').triggerHandler('blur');
        scope.$digest();
        expect(form.$valid).toBeFalsy();
    });

    it('should validate value as it is unique', function () {
        scope.result = true;
        element.find('input').triggerHandler('blur');
        scope.$digest();
        expect(form.$valid).toBeTruthy();
    });
});
