'use strict';

var mountFolder = function (connect, dir) {
    return connect.static(require('path').resolve(dir));
};

function parent(child) {
    return child.substring(0, child.lastIndexOf('/'));
}

module.exports = function (grunt) {
    // load all grunt tasks
    require('matchdep').filterDev('grunt-*').forEach(grunt.loadNpmTasks);
	require('time-grunt')(grunt);

    // configurable paths
    var config = {
        core: 'core/app',
        libs: 'common/lib',
        app: 'walli/app',
        dist: 'dist'
    };

    var serverPort = grunt.option('serverPort') || 9005;
    var reloadPort = grunt.option('reloadPort') || 8889;
    var hostname = grunt.option('hostname') || 'localhost';

    var lrSnippet = require('connect-livereload')({ port: reloadPort });
    var proxySnippet = require('grunt-connect-proxy/lib/utils').proxyRequest;

    grunt.initConfig({
        yeoman: config,
        watch: {
            compass: {
                files: ['<%= yeoman.app %>/css/{,*/}*.{scss,sass}'],
                tasks: ['compass:app']
            },
            livereload: {
                options: {
                    livereload: config.reloadPort
                },
                files: [
                    '<%= yeoman.app %>/{,*/}*.html',
                    '{.tmp,<%= yeoman.app %>}/css/{,*/}*.css',
                    '{.tmp,<%= yeoman.app %>}/js/{,*/}*.js',
                    '<%= yeoman.app %>/img/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                ]
            }
        },
        connect: {
            proxies: [
                {
                    context: '/api',
                    host: 'localhost',
                    port: 9002,
                    https: false,
                    changeOrigin: true
                }
            ],
            options: {
                port: serverPort,
                hostname: hostname
            },
            livereload: {
                options: {
                    middleware: function (connect) {
                        return [
                            proxySnippet,
                            lrSnippet,
                            mountFolder(connect, '.tmp'),
                            mountFolder(connect, config.app)
                        ];
                    }
                }
            },
            dist: {
                options: {
                    middleware: function (connect) {
                        return [
                            proxySnippet,
                            mountFolder(connect, config.dist)
                        ];
                    }
                }
            }
        },
        open: {
            server: {
                url: 'http://localhost:<%= connect.options.port %>'
            }
        },
        clean: {
            core: [config.core + '/js/templates.js', parent(config.core) + '/coverage'],
            app: [config.app + '/js/templates.js', parent(config.app) + '/coverage'],
            dist: config.dist,
            runtime: ['.tmp', '.sass-cache', 'coverage'],
            libs: config.libs + '/walli-core/*.js'
        },
        concurrent: {
            core: [
                'jshint:core',
                'ngtemplates:core',
                'karma:core'
            ],
            app: [
                'jshint:app',
                'karma:app',
                'ngtemplates:app',
                'concat:angular',
                'concat:modules',
                'concat:plugins',
                'concat:charts'
            ]
        },
        jshint: {
            options: {
                jshintrc: '.jshintrc',
                reporter: 'checkstyle'
            },
            core: {
                options: {
                    reporterOutput: 'core/results/jshint/jshint.xml'
                },
                files: {
                    src: ['<%= yeoman.core %>/js/{,*/}*.js']
                }
            },
            app: {
                options: {
                    reporterOutput: 'walli/results/jshint/jshint.xml'
                },
                files: {
                    src: [
                        '<%= yeoman.app %>/js/{,*/}*.js',
                        '!<%= yeoman.app %>/js/core.js'
                    ]
                }
            }
        },
        karma: {
            options: {
                singleRun: true
            },
            core: {
                configFile: parent(config.core) + '/conf/karma.conf.js'
            },
            app: {
                configFile: parent(config.app) + '/conf/karma.conf.js'
            }
        },
        ngtemplates: {
            core: {
                options: {
                    base: '<%= yeoman.core %>',
                    module: 'core'
                },
                src: '<%= yeoman.core %>/partials/**.html',
                dest: '<%= yeoman.core %>/js/templates.js'
            },
            app: {
                options: {
                    base: '<%= yeoman.app %>',
                    module: 'walli'
                },
                src: '<%= yeoman.app %>/partials/{,*/}*.html',
                dest: '<%= yeoman.app %>/js/templates.js'
            }
        },
        compass: {
            options: {
                cssDir: '.tmp/css',
                generatedImagesDir: '<%= yeoman.dist %>/img',
                imagesDir: '<%= yeoman.app %>/img',
                javascriptsDir: '<%= yeoman.app %>/js',
                importPath: '<%= yeoman.libs %>',
                relativeAssets: false
            },
            app: {
                options: {
                    fontsDir: '<%= yeoman.libs %>/font-awesome/font',
                    sassDir: '<%= yeoman.app %>/css',
                    imagesDir: '<%= yeoman.app %>/img',
                    javascriptsDir: '<%= yeoman.app %>/js'
                }
            },
            dist: {}
        },
        concat: {
            angular: {
                files: {
                    'dist/js/angular.js': [
                        '<%=yeoman.libs %>/angular/angular.min.js']
                }
            },
            modules: {
                files: {
                    'dist/js/modules.js': [
                        '<%=yeoman.libs %>/angular-route/angular-route.min.js',
                        '<%=yeoman.libs %>/angular-resource/angular-resource.min.js',
                        '<%=yeoman.libs %>/angular-cookies/angular-cookies.min.js',
                        '<%=yeoman.libs %>/angular-sanitize/angular-sanitize.min.js' ]
                }
            },
            charts: {
                files: {
                    'dist/js/charts.js': [
                        '<%=yeoman.libs %>/d3/d3.min.js',
                        '<%=yeoman.libs %>/bower-xcharts/xcharts.min.js',
                        '<%=yeoman.libs %>/justgage/justgage.js',
                        '<%=yeoman.libs %>/raphael/raphael-min.js'
                    ]
                }
            },
            plugins: {
                files: {
                    'dist/js/plugins.js': [
                        '<%=yeoman.libs %>/jquery/jquery.js',
                        '<%=yeoman.libs %>/underscore/underscore.js',
                        '<%=yeoman.libs %>/bootstrap-sass/js/modal.js',
                        '<%=yeoman.libs %>/bootstrap-sass/js/collapse.js',
                        '<%=yeoman.libs %>/bootstrap-sass/js/dropdown.js',
                        '<%=yeoman.libs %>/gritter/js/jquery.gritter.js'
                    ]
                }
            },
            core: {
                files: {
                    '.tmp/js/core.js': [
                        '<%=yeoman.core %>/js/core.js',
                        '<%=yeoman.core %>/js/**/*.js']
                }
            },
            app: {
                files: {
                    '.tmp/js/app.js': [
                        '<%=yeoman.app %>/js/walli.js',
                        '<%=yeoman.app %>/js/{,*/}*.js',
                        '!<%=yeoman.app %>/js/core.js',
                        '<%=yeoman.app %>/js/**/*.js']
                }
            }
        },
        copy: {
            app: {
                files: [
                    {
                        expand: true,
                        dot: true,
                        cwd: '<%= yeoman.app %>',
                        dest: '<%= yeoman.dist %>',
                        src: [
                            '*.{ico,png,txt}',
                            '.htaccess',
                            'img/{,*/}*.{gif,webp,svg}',
                            'css/fonts/*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '.tmp/img',
                        dest: '<%= yeoman.dist %>/img',
                        src: [
                            'generated/*'
                        ]
                    },
                    {
                        expand: true,
                        cwd: '<%= yeoman.libs %>/font-awesome/font',
                        dest: '<%= yeoman.dist %>/font',
                        src: [
                            '*'
                        ]
                    }
                ]
            },
            js: {
                files: [
                    {
                        expand: true,
                        flatten: true,
                        cwd: parent(config.app),
                        dest: '<%=yeoman.dist %>',
                        src: [
                            'js/*'
                        ]
                    }
                ]
            }
        },
        imagemin: {
            app: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>/img',
                        src: '{,*/}*.{png,jpg,jpeg}',
                        dest: '<%= yeoman.dist %>/img'
                    }
                ]
            }
        },
        htmlmin: {
            app: {
                files: [
                    {
                        expand: true,
                        cwd: '<%= yeoman.app %>',
                        src: ['*.html'],
                        dest: '<%= yeoman.dist %>'
                    }
                ]
            }
        },
        cdnify: {
            dist: {
                html: ['<%= yeoman.dist %>/*.html']
            }
        },
        uglify: {
            options: {
                banner: '/*! Built by walli on <%= grunt.template.today("dd-mm-yyyy HH:MM:ss") %> */\n'
            },
            modules: {
                files: {
                    '<%= yeoman.dist %>/js/modules.js': [
                        '<%= yeoman.dist %>/js/modules.js'
                    ]
                }
            },
            charts: {
                files: {
                    '<%= yeoman.dist %>/js/charts.js': [
                        '<%= yeoman.dist %>/js/charts.js'
                    ]
                }
            },
            plugins: {
                files: {
                    '<%= yeoman.dist %>/js/plugins.js': [
                        '<%= yeoman.dist %>/js/plugins.js'
                    ]
                }
            },
            core: {
                files: {
                    '<%= yeoman.dist %>/js/core.js': [
                        '<%= yeoman.dist %>/js/core.js'
                    ]
                }
            },
            app: {
                files: {
                    '<%= yeoman.dist %>/js/app.js': [
                        '<%= yeoman.dist %>/js/app.js'
                    ]
                }
            }
        },
        ngmin: {
            dist: {
                files: [
                    {
                        expand: true,
                        cwd: '.tmp/js',
                        src: '*.js',
                        dest: '<%= yeoman.dist %>/js'
                    }
                ]
            }
        },
        cssmin: {
            dist: {
                files: {
                    '<%= yeoman.dist %>/css/walli.css': [
                        '.tmp/css/walli.css',
                        '<%= yeoman.dist %>/css/walli.css'
                    ]
                }
            }
        },
        rev: {
            dist: {
                files: {
                    src: [
                        '<%= yeoman.dist %>/js/{,*/}*.js',
                        '<%= yeoman.dist %>/css/{,*/}*.css',
                        '<%= yeoman.dist %>/images/{,*/}*.{png,jpg,jpeg,gif,webp,svg}'
                    ]
                }
            }
        },
        usemin: {
            html: ['<%= yeoman.dist %>/{,*/}*.html'],
            css: ['<%= yeoman.dist %>/css/{,*/}*.css'],
            options: {
                dirs: ['<%= yeoman.dist %>']
            }
        }
    });

    grunt.registerTask('server', function (target) {
        if (target === 'dist') {
            return grunt.task.run(['build', 'open', 'configureProxies','connect:dist:keepalive']);
        }

        grunt.task.run([
            'clean:runtime',
            'configureProxies',
            'ngtemplates:core',
            'compass:app',
            'concat:core',
            'connect:livereload',
            'open',
            'watch'
        ]);
    });


    grunt.registerTask('build', [
        'clean',
        'concurrent:core',
        'concat:core',
        'concurrent:app',
        'concat:app',
        'check-coverage:core',
        'compass:app',
        'copy:app',
        'imagemin',
        'htmlmin',
        'cdnify',
        'ngmin',
        'uglify',
        'cssmin',
        'rev' ,
        'usemin',
        'copy:js'
    ]);

    grunt.registerTask('check-coverage', function(module){
        var done = this.async();
        var options = {
            cmd: 'istanbul',
            args: ['check-coverage', '--functions=100', '--statements=100', '--branches=100', '--lines=100', '--dir=coverage', '--root=' + module]}
        grunt.util.spawn(options, function(error, result, code){
            if(code == 8) {
                var errors = result.stderr.match(/\nERROR:(.*)/g);
                for (var i = 0; i < errors.length; i++) {
                    grunt.log.error(errors[i]);
                }
            }
            done();
        });
    });

    grunt.registerTask('default', 'build');
};
